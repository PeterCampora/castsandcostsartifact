from .rtypes import *
from . import flags, logging
from .exc import UnknownTypeError, UnexpectedTypeError
import pdb
import math
import itertools

firstLetter = (ord('A') - 1)
tvar = 0
cvar = 0#(ord('a') - 1)
def fresh_choice_name():
    global cvar
    cvar += 1
    #cname = chr(cvar)
    return str(cvar)

def fresh_tvar():
    global tvar
    tvar += 1    
    return TypeVariable(tvar)

def ftv(ty):
    if (tyinstance(ty, Function)):
        #check dom and cod        
        if pinstance(ty.froms, DynParameters):
            return ftv(ty.to)
        elif pinstance(ty.froms, AnonymousParameters):
            newTys = []
            for t in ty.froms.parameters:
                newTys += ftv(t)

            newTys = ftv(ty.to)
            return newTys
        elif pinstance(ty.froms, NamedParameters):
            newTys =  []
            for n,t in ty.froms.parameters:
                newTys += ftv(t)
            
            newTys += ftv(ty.to)
            return newTys
        else:
            raise UnknownTypeError()    
    if (tyinstance(ty, Tuple)):
        #check components
        newEls = []
        for e in ty.elements:
            newEls += (ftv(e))
        return newEls            
    elif (tyinstance(ty, List)):
        #check underlying type
        underlying = ftv(ty.type)
        return (underlying)
    elif (tyinstance(ty, Dict)):
        #check key and value types
        return ftv(ty.keys) + ftv(ty.values)
    elif (tyinstance(ty, Object)):
        #check types of attributes
        mems = []
        for n in ty.members:
            mems += (ftv(n))        
        return mems
    elif (tyinstance(ty, Class)):
        #check methods and attributes
        mems = []
        for n in ty.members:
            mems += (ftv(n))
        return mems
    elif (tyinstance(ty, Choice)):
        lty = ftv(selL(ty.name, ty))
        rty = ftv(selR(ty.name, ty))
        return lty + rty
    elif (tyinstance(ty, TypeVariable)):
        return [ty]
    else:
        return []

def permute_choices(n):
    global cvar
    selectors = [str(i) for i in range(1,cvar+1)]
    return itertools.combinations(selectors, n)

def apply_decisions(decision, cost, pat):
    c = cost
    p = pat
    for sel in decision:
        c = sel(c)
        p = sel(p)
    if (not p):
        # if the selection on the pattern returns False, return infinity for the cost
        return float("inf")
    while (tyinstance(c, Choice)):
        #otherwise get rid of all of the remaining choices by setting them as if the parameter was Dyn
        c = selL(c.name, c)
        
    return c

def find_lowest_cost_perms(cost, pat):
    #collect the n choose 2 decisions adding 2 new type annotations
    #global cvar
    min_cost = float("inf")
    min_cost_val = float("inf")
    min_desc = (None, None)
    min_d_func = (None, None)
    relevantDecisions = collect_choices(cost)
    decisions3 = itertools.combinations(relevantDecisions, 3)
    decisions2 = itertools.combinations(relevantDecisions, 2)
    decisions1 = map(lambda x: [x], relevantDecisions)
    #decisions3 = permute_choices(3)
    #decisions2 = permute_choices(2)
    #decisions1 = [[str(i)] for i in range(1,cvar+1)]
    decisions, dec = itertools.tee(itertools.chain(decisions3, decisions2, decisions1), 2)
    for d in decisions:
        dFunc = map(lambda y: (lambda x: selR(y, x)), d)
        curr_cost = apply_decisions(dFunc, cost, pat)
        c = curr_cost
        if (not isinstance(c, float)):
            for n in c.free_symbols:
                c = c.subs(n, 10000)        
        if (c <= min_cost_val):
            min_cost = curr_cost
            min_cost_val = c
            min_desc = d
            min_d_func = dFunc
    dwco = []
    for s in dec:
        dFunc = map(lambda y: (lambda x: selR(y, x)), s)
        curr_cost = apply_decisions(dFunc, cost, pat)
        c = curr_cost
        if (not isinstance(c, float)):
            for n in c.free_symbols:
                c = c.subs(n, 10000)
        if (s != min_desc):
            if ((c / min_cost_val) < 1.5):                
                dwco.append(s)
    return (min_cost, min_desc, min_d_func, dwco)

def collect_choices(cost):
    names = set()
    def collect_choices_help(c):
        if (tyinstance(c, Choice)):
            names.add(c.name)
            collect_choices_help(selL(c.name, c))
            collect_choices_help(selR(c.name, c))

    collect_choices_help(cost)
    return names

def ftvEnv(env):
    tys = []
    for t in env.values():
        ts = ftv(t)
        tys += ts
    return tys

def generalize(ty, env):
    ft = set(ftv(ty))
    fe = set(ftvEnv(env))
    fvs = list(ft - fe)
    return TypeScheme(fvs, ty)

def instantiate(tys):
    newSub = {}
    for var in tys.tvars:
        newSub[var] = fresh_tvar()
    return applySub(tys.ty, newSub)
    
def tvars_to_dyn(ty):
    if (tyinstance(ty, TypeScheme)):
        return tvars_to_dyn(ty.ty)
    if (tyinstance(ty, Function)):
        #check dom and cod        
        if pinstance(ty.froms, DynParameters):
            return Function(ty.froms, tvars_to_dyn(ty.to))
        elif pinstance(ty.froms, AnonymousParameters):
            newTys = []
            for t in ty.froms.parameters:
                newTys.append(tvars_to_dyn(t))
            froms = AnonymousParameters(newTys)
            to = tvars_to_dyn(ty.to)
            return Function(froms, to)
        elif pinstance(ty.froms, NamedParameters):
            newTys =  []
            for n,t in ty.froms.parameters:
                newTys.append((n,tvars_to_dyn(t)))
            froms = NamedParameters(newTys)
            to = tvars_to_dyn(ty.to)
            return Function(froms, to)            
        else:
            raise UnknownTypeError()    
    if (tyinstance(ty, Tuple)):
        #check components
        newEls = []
        for e in ty.elements:
            newEls.append(tvars_to_dyn(e))
        return Tuple(*newEls)            
    elif (tyinstance(ty, List)):
        #check underlying type
        underlying = tvars_to_dyn(ty.type)
        return List(underlying)
    elif (tyinstance(ty, Dict)):
        #check key and value types
        return Dict(tvars_to_dyn(ty.keys), tvars_to_dyn(ty.values))
    elif (tyinstance(ty, Object)):
        #check types of attributes
        mems = []
        for n in ty.members:
            mems.append(tvars_to_dyn(n))
        ty.members = mems #mutate ty
        return ty
    elif (tyinstance(ty, Class)):
        #check methods and attributes
        mems = []
        for n in ty.members:
            mems.append(tvars_to_dyn(n))
        ty.members = mems #mutate ty
        return ty
    elif (tyinstance(ty, Choice)):
        lty = tvars_to_dyn(selL(ty.name, ty))
        rty = tvars_to_dyn(selR(ty.name, ty))
        return makeChoice(ty.name, lty, rty)
    elif (tyinstance(ty, TypeVariable)):
        return Dyn
    else:
        return ty

def info_join(ty1, ty2):
    def memjoin(m1, m2):
        mems = {}
        for k in m1:
            if k in m2:
                mems[k] = ijoin(m1[k], m2[k])
            else: mems[k] = m1[k]
        for k in m2:
            if k not in m1:
                mems[k] = m2[k]
        return mems

    def ijoin(ty1, ty2):
        assert isinstance(ty1, PyType)
        assert isinstance(ty2, PyType)
        if not ty1.top_free() and ty2.top_free():
            return InfoTop
        elif tyinstance(ty1, Dyn):
            return ty2
        elif tyinstance(ty2, Dyn):
            return ty1
        elif ty1 == ty2:
            return ty1
        elif tyinstance(ty1, Function) and tyinstance(ty2, Function):
            return Function(info_paramjoin(ty1.froms, ty2.froms), ijoin(ty1.to, ty2.to))
        elif tyinstance(ty1, Object) and tyinstance(ty2, Object):
            name = ty1.name if ty1.name == ty2.name else ''
            ty1 = ty1.substitute(ty1.name, TypeVariable(name), False)
            ty2 = ty2.substitute(ty2.name, TypeVariable(name), False)
            mems = memjoin(ty1.members, ty2.members)  
            return Object(name, mems)
        elif tyinstance(ty1, Class) and tyinstance(ty2, Class):
            name = ty1.name if ty1.name == ty2.name else ''
            ty1 = ty1.substitute(ty1.name, TypeVariable(name), False)
            ty2 = ty2.substitute(ty2.name, TypeVariable(name), False)
            mems = memjoin(ty1.members, ty2.members)
            inst = memjoin(ty1.instance_members, ty2.instance_members)
            return Class(name, mems, inst)
        elif tyinstance(ty1, List) and tyinstance(ty2, List):
            return List(ijoin(ty1.type, ty2.type))
        elif tyinstance(ty1, Set) and tyinstance(ty2, Set):
            return Set(ijoin(ty1.type, ty2.type))
        elif tyinstance(ty1, Dict) and tyinstance(ty2, Dict):
            return Dict(ijoin(ty1.keys, ty2.keys), ijoin(ty1.values, ty2.values))
        elif tyinstance(ty1, Tuple) and tyinstance(ty2, Tuple):
            if len(ty1.elements) == len(ty2.elements):
                return Tuple(*[ijoin(e1, e2) for (e1, e2) in zip(ty1.elements, ty2.elements)])
            else: return InfoTop
        elif tyinstance(ty1, Structural) and tyinstance(ty2, Structural) and \
             (tyinstance(ty1, Object) or tyinstance(ty2, Object)):
            return ijoin(ty1.structure(), ty2.structure())
        else: return InfoTop
    join = ijoin(ty1, ty2)
    #pdb.set_trace()
    if join.top_free():
        return join
    else: return InfoTop

def n_info_join(*types):
    if type(types[0]) == list and len(types) == 1:
        types = types[0]
    if len(types) == 0:
        return Dyn
    join = types[0]
    for ty in types[1:]:
        join = info_join(join, ty)
        if not join.top_free() or not ty.top_free():
            return InfoTop
    return join

def info_paramjoin(p1, p2):
    if pinstance(p1, DynParameters):
        return p2
    elif pinstance(p2, DynParameters):
        return p1
    elif pinstance(p1, NamedParameters):
        if len(p1.parameters) != len(p2.parameters):
            return InfoTop
        elif pinstance(p2, NamedParameters):
            if all(k1 == k2 for (k1, _), (k2, _) in zip(p1.parameters, p2.parameters)):
                return NamedParameters([(k1, info_join(t1, t2)) for (k1, t1), (_, t2) in\
                                            zip(p1.parameters, p2.parameters)])
            else: return InfoTop
        elif pinstance(p2, AnonymousParameters):
            return AnonymousParameters([info_join(t1, t2) for t1, (_, t2) in\
                                            zip(p2.parameters, p1.parameters)])
        else: raise UnknownTypeError()
    elif pinstance(p1, AnonymousParameters):
        if len(p1.parameters) != len(p2.parameters):
            return InfoTop
        elif pinstance(p2, NamedParameters):
            return AnonymousParameters([info_join(t1, t2) for t1, (_, t2) in\
                                            zip(p1.parameters, p2.parameters)])
        elif pinstance(p2, AnonymousParameters):
            return AnonymousParameters([info_join(t1, t2) for t1, t2 in\
                                            zip(p1.parameters, p2.parameters)])
        else: raise UnknownTypeError()
    else: raise UnknownTypeError()

def prim_subtype(t1, t2):
    prims = [Bool, Int, Float, Complex]
    t1tys = [tyinstance(t1, ty) for ty in prims]
    t2tys = [tyinstance(t2, ty) for ty in prims]
    if not(any(t1tys)) or not(any(t2tys)):
        return False
    return t1tys.index(True) <= t2tys.index(True)

def primjoin(tys, min=Int, max=Complex):
    try:
        ty = tys[0]
        for ity in tys[1:]:
            if prim_subtype(ty, ity):
                ty = ity
        if not prim_subtype(ty, max):
            return Dyn
        if prim_subtype(ty, min):
            return min
        else: return ty
    except UnexpectedTypeError:
        return Dyn
    except IndexError:
        return Dyn

def binop_type(l, op, r):
    if tyinstance(l, InferBottom) or tyinstance(r, InferBottom):
        return {}, False, InferBottom
    if not flags.MORE_BINOP_CHECKING and (tyinstance(l, Dyn) or tyinstance(r, Dyn)):
        return {}, True, Dyn
    #pdb.set_trace()

    def prim(ty):
        return any(tyinstance(ty, t) for t in [Bool, Int, Float, Complex])
    def intlike(ty):
        return any(tyinstance(ty, t) for t in [Bool, Int])
    def arith(op):
        return any(isinstance(op, o) for o in [ast.Add, ast.Mult, ast.Div, ast.FloorDiv, ast.Sub, ast.Pow])
    def shifting(op):
        return any(isinstance(op, o) for o in [ast.LShift, ast.RShift])
    def logical(op):
        return any(isinstance(op, o) for o in [ast.BitOr, ast.BitAnd, ast.BitXor])
    def listlike(ty):
        return any(tyinstance(ty, t) for t in [List, String])

    if isinstance(op, ast.FloorDiv):
        if any(tyinstance(nd, ty) for nd in [l, r] for ty in [String, Complex, List, Tuple, Dict]):
            return {}, True, InfoTop
    if isinstance(op, ast.Mod):
        if any(tyinstance(l, ty) for ty in [Complex, List, Tuple, Dict]):
            return {}, True, InfoTop
    if shifting(op) or logical(op):
        if any(tyinstance(nd, ty) for nd in [l, r] for ty in [Float, Complex, String, List, Tuple, Dict]):
            return {}, True, InfoTop
    if arith(op):
        if tyinstance(l, TypeVariable) or tyinstance(r, TypeVariable):
            return unify(l, r)
        if tyinstance(l, Choice):
           #pdb.set_trace()
           new_r = makeChoice(l.name, r, r)
           lsub, lpat, lty = binop_type(selL(l.name, l), op, selL(l.name, new_r))
           rsub, rpat, rty = binop_type(selR(l.name, l), op,  selR(l.name, new_r))
           sub = mergeSubs(l.name, lsub, rsub)
           ty = applySub(makeChoice(l.name, lty, rty), sub)
           return sub, makeChoice(l.name, lpat, rpat), ty
        if tyinstance(r, Choice):
           return binop_type(r, op,  l)
        if (tyinstance(l, Float)):
            if any(tyinstance(r, ty) for ty in [Float, Int, Dyn]):
                return {}, True, Float            
        if (tyinstance(r, Float)):
            if any(tyinstance(l, ty) for ty in [Float, Int, Dyn]):
                return {}, True, Float
        if any(tyinstance(nd, ty) for nd in [l, r] for ty in [Dict]):
            return {}, True, InfoTop
        if not isinstance(op, ast.Add) and not isinstance(op, ast.Mult) and \
                any(tyinstance(nd, ty) for nd in [l, r] for ty in [String, List, Tuple]):
            return {}, True, InfoTop
    if any(tyinstance(nd, ty) for nd in [l, r] for ty in [Object, Dyn]):
        return {}, True, Dyn
    
    if tyinstance(l, Bool):
        if arith(op) or shifting(op) or isinstance(op, ast.Mod):
            if isinstance(op, ast.Div) and prim_subtype(r, Float):
                return {}, True, Float
            if tyinstance(r, Bool):
                return {}, True, Int
            elif prim(r):
                return {}, True, r
            elif listlike(r) and isinstance(op, ast.Mult):
                return {}, True, r
            elif tyinstance(r, Tuple) and isinstance(op, ast.Mult):
                return {}, True, Dyn
            else: return {}, True, InfoTop
        elif logical(op):
            return {}, True, r
        else:
            return {}, True, InfoTop
    elif tyinstance(l, Int):
        if isinstance(op, ast.Div) and prim_subtype(r, Float):
            return {}, True, Float
        elif listlike(r) and isinstance(op, ast.Mult):
            return {}, True, r
        elif isinstance(r, Tuple) and isinstance(op, ast.Mult):
            return {}, True, Dyn
        elif prim_subtype(r, l):
            return {}, True, l
        elif prim_subtype(l, r):
            return {}, True, r
        else:
            return {}, True, InfoTop
    elif prim(l):
        if prim_subtype(r, l):
            return {}, True, l
        elif prim_subtype(l, r):
            return {}, True, r
        else:
            return {}, True, InfoTop
    elif listlike(l):
        if intlike(r) and isinstance(op, ast.Mult):
            return {}, True, l
        elif tyinstance(l, String) and isinstance(op, ast.Mod):
            return {}, True, l
        elif any(tyinstance(l, ty) and tyinstance(r, ty) for ty in [List, String]) and isinstance(op, ast.Add):
            return {}, True, tyjoin([l, r])
        else:
            return {}, True, InfoTop
    elif tyinstance(l, Tuple):
        if intlike(r) and isinstance(op, ast.Mult):
            return {}, True, Dyn
        elif tyinstance(r, Tuple) and isinstance(op, ast.Add):
            return {}, True, Tuple(*(l.elements + r.elements))
        else:
            return {}, True, InfoTop
    else:
        return {}, True, Dyn

def subcompat(ty1, ty2, env=None, ctx=None):
    if env == None:
        env = {}

    if not ty1.top_free() or not ty2.top_free():
        return False

    return subtype(env, ctx, merge(ty1, ty2), ty2)

def normalize(ty):
    if ty == int:
        return Int
    elif ty == bool:
        return Bool
    elif ty == float:
        return Float
    elif ty == type(None):
        return Void
    elif ty == complex:
        return Complex
    elif ty == str:
        return String
    elif ty == None:
        return Dyn
    elif isinstance(ty, tuple):
        return Tuple(*[normalize(t) for t in ty])
    elif isinstance(ty, dict):
        nty = {}
        for k in ty:
            if type(k) != str:
                raise UnknownTypeError()
            nty[k] = normalize(ty[k])
        return Object('', nty)
    elif ty is Object:
        return normalize(Object('', {}))
    elif ty is Class:
        return normalize(Class('', {}, {}))
    elif tyinstance(ty, Object):
        nty = {}
        for k in ty.members:
            if type(k) != str:
                raise UnknownTypeError()
            nty[k] = normalize(ty.members[k])
        return Object(ty.name, nty)
    elif tyinstance(ty, Class):
        nty = {}
        for k in ty.members:
            if type(k) != str:
                raise UnknownTypeError()
            nty[k] = normalize(ty.members[k])
        ity = {}
        for k in ty.instance_members:
            if type(k) != str:
                raise UnknownTypeError()
            ity[k] = normalize(ty.instance_members[k])
        return Class(ty.name, nty, ity)
    elif tyinstance(ty, Tuple):
        return Tuple(*[normalize(t) for t in ty.elements])
    elif tyinstance(ty, Function):
        return Function(normalize_params(ty.froms), normalize(ty.to))
    elif tyinstance(ty, Dict):
        return Dict(normalize(ty.keys), normalize(ty.values))
    elif tyinstance(ty, List):
        return List(normalize(ty.type))
    elif tyinstance(ty, Set):
        return Set(normalize(ty.type))
    elif tyinstance(ty, Iterable):
        return Iterable(normalize(ty.type))
    elif isinstance(ty, PyType):
        return ty
    else: raise UnknownTypeError(ty)

def normalize_params(params):
    if pinstance(params, AnonymousParameters):
        return AnonymousParameters([normalize(p) for p in params.parameters])
    elif pinstance(params, NamedParameters):
        return NamedParameters([(k, normalize(p)) for k,p in params.parameters])
    elif pinstance(params, DynParameters):
        return params
    else: raise UnknownTypeError()

def widen(*types):
    join = tyjoin(types)

def tyjoin(*types):
    if isinstance(types[0], list) and len(types) == 1:
        types = types[0]
    if len(types) == 0:
        return Dyn
    if all(tyinstance(x, InferBottom) for x in types):
        return InferBottom
    types = [ty for ty in types if not tyinstance(ty, InferBottom)]
    if len(types) == 0:
        return Dyn
    join = types[0]
    if tyinstance(join, Dyn):
        return Dyn
    for ty in types[1:]:
        if not flags.FLAT_PRIMITIVES:
            pjoin = primjoin([join, ty])
            if not tyinstance(pjoin, Dyn):
                join = pjoin

        if not tyinstance(ty, join.__class__) or \
                tyinstance(ty, Dyn):
            return Dyn
        elif tyinstance(ty, TypeVariable):
            if ty.name == join.name:
                continue
            else: return Dyn
        elif tyinstance(ty, List):
            join = List(tyjoin([ty.type, join.type]))
        elif tyinstance(ty, Tuple):
            if len(ty.elements) == len(join.elements):
                join = Tuple(*[tyjoin(list(p)) for p in zip(ty.elements, join.elements)])
            else: return Dyn
        elif tyinstance(ty, Dict):
            join = Dict(tyjoin([ty.keys, join.keys]), tyjoin([ty.values, join.values]))
        elif tyinstance(ty, Function):
            join = Function(paramjoin(ty.froms, join.froms), 
                            tyjoin([ty.to, join.to]))
        elif tyinstance(ty, Choice):
            join = Choice(ty.name, tyjoin(selL(ty.name, ty), join), tyjoin(selR(ty.name, ty), join))
        elif tyinstance(ty, Object) or tyinstance(ty, Class):
            name = ty.name if ty.name == join.name else ''
            members = {}
            for x in ty.members:
                if x in join.members:
                    members[x] = tyjoin([ty.members[x], join.members[x]])
            if tyinstance(ty, Class) and tyinstance(join, Class):
                imems = {}
                for x in ty.instance_members:
                    if x in join.instance_members:
                        imems[x] = tyjoin([ty.instance_members[x], join.instance_members[x]])
                join = Class(name, members, imems)
            else: join = ty.__class__(name,members)
        if join == Dyn: return Dyn
    return join

def paramjoin(p1, p2):
    if pinstance(p1, DynParameters):
        return p1
    elif pinstance(p2, DynParameters):
        return p2
    elif pinstance(p1, NamedParameters):
        if len(p1.parameters) != len(p2.parameters):
            return DynParameters
        elif pinstance(p2, NamedParameters):
            if all(k1 == k2 for (k1, _), (k2, _) in zip(p1.parameters, p2.parameters)):
                return NamedParameters([(k1, tyjoin(t1, t2)) for (k1, t1), (_, t2) in\
                                            zip(p1.parameters, p2.parameters)])
            else: 
                return AnonymousParameters([tyjoin(t1, t2) for (_, t1), (_, t2) in\
                                                zip(p1.parameters, p2.parameters)])
        elif pinstance(p2, AnonymousParameters):
            return AnonymousParameters([tyjoin(t1, t2) for t1, (_, t2) in\
                                            zip(p2.parameters, p1.parameters)])
        else: raise UnknownTypeError()
    elif pinstance(p1, AnonymousParameters):
        if len(p1.parameters) != len(p2.parameters):
            return DynParameters
        elif pinstance(p2, NamedParameters):
            return AnonymousParameters([tyjoin(t1, t2) for t1, (_, t2) in\
                                             zip(p1.parameters, p2.parameters)])
        elif pinstance(p2, AnonymousParameters):
            return AnonymousParameters([tyjoin(t1, t2) for t1, t2 in\
                                             zip(p1.parameters, p2.parameters)])
        else: raise UnknownTypeError()
    else: raise UnknownTypeError()

def shallow(ty):
    return ty.__class__
    
def param_subtype(env, ctx, p1, p2):
    
    if p1 == p2:
        return True
    elif pinstance(p1, NamedParameters):
        if pinstance(p2, NamedParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(((k1 == k2 or not flags.PARAMETER_NAME_CHECKING) and subtype(env, ctx, f2, f1)) for\
                        (k1,f1), (k2,f2) in zip(p1.parameters, p2.parameters)) # Covariance handled here
        elif pinstance(p2, AnonymousParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(subtype(env, ctx, f2, f1) for (_, f1), f2 in\
                        zip(p1.parameters, p2.parameters))
        else: return False
    elif pinstance(p1, AnonymousParameters):
        if pinstance(p2, AnonymousParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(subtype(env, ctx, f2, f1) for f1, f2 in zip(p1.parameters,
                                                                p2.parameters))
        elif pinstance(p1, NamedParameters):
            return len(p1.parameters) == len(p2.parameters) and len(p1.parameters) == 0
        else: return False
    else: return False
        
def param_equal(env, ctx, p1, p2):
    if p1 == p2:
        return True
    elif pinstance(p1, NamedParameters):
        if pinstance(p2, NamedParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(((k1 == k2 or not flags.PARAMETER_NAME_CHECKING) and equal(env, ctx, f2, f1)) for\
                        (k1,f1), (k2,f2) in zip(p1.parameters, p2.parameters)) # Covariance handled here
        elif pinstance(p2, AnonymousParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(equal(env, ctx, f2, f1) for (_, f1), f2 in\
                        zip(p1.parameters, p2.parameters))
        else: return False
    elif pinstance(p1, AnonymousParameters):
        if pinstance(p2, AnonymousParameters):
            return len(p1.parameters) == len(p2.parameters) and\
                all(equal(env, ctx, f2, f1) for f1, f2 in zip(p1.parameters,
                                                                p2.parameters))
        elif pinstance(p1, NamedParameters):
            return len(p1.parameters) == len(p2.parameters) and len(p1.parameters) == 0
        else: return False
    else: return False
        
def subtype(env, ctx, ty1, ty2):
    if not flags.FLAT_PRIMITIVES and prim_subtype(ty1, ty2):
        return True
    elif ty1 == ty2:
        return True
    elif tyinstance(ty2, InfoTop):
        return True
    elif tyinstance(ty1, InferBottom) or tyinstance(ty2, InferBottom):
        return True
    elif tyinstance(ty2, Bytes):
        return tyinstance(ty1, Bytes)
    elif tyinstance(ty2, List):
        if tyinstance(ty1, List):
            return equal(env, ctx, ty1.type, ty2.type)
        else: return False
    elif tyinstance(ty2, Tuple):
        if tyinstance(ty1, Tuple):
            return len(ty1.elements) == len(ty2.elements) and \
                all(equal(env, ctx, e1, e2) for e1, e2 in zip(ty1.elements, ty2.elements))
        else: return False
    elif tyinstance(ty2, Function):
        if tyinstance(ty1, Function):
            return param_subtype(env, ctx, ty1.froms, ty2.froms) and subtype(env, ctx, ty1.to, ty2.to) # Covariance DOES NOT happen here, it's in param_subtype
        elif tyinstance(ty1, Class):
            if '__new__' in ty1.members:
                fty = ty1.member_type('__new__')
                if tyinstance(fty, Dyn):
                    fty = fty.bind()
            elif '__init__' in ty1.members:
                fty = ty1.member_type('__init__')
                if tyinstance(fty, Dyn):
                    fty = fty.bind()
            else: fty = Function(DynParameters, ty1.instance())
            return subtype(env, ctx, fty, ty2)
        elif tyinstance(ty1, Object):
            if '__call__' in ty1.members:
                return subtype(env, ctx, ty1.member_type('__call__'), ty2)
            else: return False
        else: return False
    elif tyinstance(ty2, Object):
        if tyinstance(ty1, Object):
            for m in ty2.members:
                if m not in ty1.members or \
                   not equal(env, ctx, 
                             # We don't want to do member_type because
                             # if theres a subclass relation btwn t1
                             # and t2 and the superclass is an alias
                             # in one of them, will do differing
                             # amounts of subsitution
                             ty1.members[m].substitute(ty1.name, TypeVariable(ty2.name), False),
                             ty2.members[m]):
                    logging.debug('Object not a subtype due to member %s: %s =/= %s' %\
                                  (m, ty1.member_type(m, None),
                                   ty2.member_type(m)), flags.SUBTY)
                    return False
            return True
        elif tyinstance(ty1, Self):
            if ctx:
                return subtype(env, ctx, ctx.instance(), ty2)
            else:
                return True
        else: return False
    elif tyinstance(ty2, Class):
        if tyinstance(ty1, Class):
            return all((m in ty1.members and \
                        equal(env, ctx, ty1.member_type(m), ty2.member_type(m))) \
                       for m in ty2.members) and \
                           all((m in ty1.instance_members and \
                                equal(env, ctx, ty1.instance_member_type(m), 
                                      ty2.instance_member_type(m)) for m in ty2.instance_members))
        else: return False
    elif tyinstance(ty2, Self):
        if ctx:
            return subtype(env, ctx, ty1, ctx.instance())
        else:
            return tyinstance(ty1, Object)
    elif tyinstance(ty2, TypeVariable):
        typ = env[ty2] if ty2 in env else Dyn
        return subtype(env, ctx, ty1, typ)
    elif tyinstance(ty1, TypeVariable):
        typ = env[ty1] if ty1 in env else Dyn
        return subtype(env, ctx, typ, ty2)
    elif tyinstance(ty1, Base):
        return tyinstance(ty2, shallow(ty1))
    else: return False

# Type equality modulo type variable unrolling -- needed for equirecursive types
def equal(env, ctx, ty1, ty2):
    if not flags.FLAT_PRIMITIVES and prim_subtype(ty1, ty2):
        return True
    elif ty1 == ty2:
        return True
    elif isinstance(ty2, TypeVariable) and tyinstance(ty1, TypeVariable):
        return ty2.name == ty1.name
    elif tyinstance(ty2, InfoTop):
        return True
    elif tyinstance(ty1, InferBottom) or tyinstance(ty2, InferBottom):
        return True
    elif tyinstance(ty2, Bytes):
        return tyinstance(ty1, Bytes)
    elif tyinstance(ty2, List):
        if tyinstance(ty1, List):
            return equal(env, ctx, ty1.type, ty2.type)
        else: return False
    elif tyinstance(ty2, Tuple):
        if tyinstance(ty1, Tuple):
            return len(ty1.elements) == len(ty2.elements) and \
                all(equal(env, ctx, e1, e2) for e1, e2 in zip(ty1.elements, ty2.elements))
        else: return False
    elif tyinstance(ty2, Function):
        if tyinstance(ty1, Function):
            return param_equal(env, ctx, ty1.froms, ty2.froms) and equal(env, ctx, ty1.to, ty2.to)
        elif tyinstance(ty1, Class):
            if '__new__' in ty1.members:
                fty = ty1.member_type('__new__')
                if tyinstance(fty, Dyn):
                    fty = fty.bind()
            elif '__init__' in ty1.members:
                fty = ty1.member_type('__init__')
                if tyinstance(fty, Dyn):
                    fty = fty.bind()
            else: fty = Function(DynParameters, ty1.instance())
            return equal(env, ctx, fty, ty2)
        elif tyinstance(ty1, Object):
            if '__call__' in ty1.members:
                return equal(env, ctx, ty1.member_type('__call__'), ty2)
            else: return False
        else: return False
    elif tyinstance(ty2, Object):
        if tyinstance(ty1, Object):
            for m in ty2.members:
                if m not in ty1.members or \
                   not equal(env, ctx, 
                             # We don't want to do member_type because
                             # if theres a subclass relation btwn t1
                             # and t2 and the superclass is an alias
                             # in one of them, will do differing
                             # amounts of subsitution
                             ty1.members[m].substitute(ty1.name, TypeVariable(ty2.name), False),
                             ty2.members[m]):
                    logging.debug('Object not equal due to member %s: %s =/= %s' %\
                                  (m, ty1.member_type(m, None),
                                   ty2.member_type(m)), flags.SUBTY)
                    return False
            return all(m in ty2.members for m in ty1.members)
        elif tyinstance(ty1, Self):
            if ctx:
                return equal(env, ctx, ctx.instance(), ty2)
            else:
                return True
        else: return False
    elif tyinstance(ty2, Class):
        if tyinstance(ty1, Class):
            return all((m in ty1.members and \
                        equal(env, ctx, ty1.member_type(m), ty2.member_type(m))) \
                       for m in ty2.members) and \
                           all((m in ty1.instance_members and \
                                equal(env, ctx, ty1.instance_member_type(m), 
                                      ty2.instance_member_type(m)) for m in ty2.instance_members)) \
                           and all(m in ty2.members for m in ty1.members) and \
                           all(m in ty2.instance_members for m in ty1.members)
        else: return False
    elif tyinstance(ty2, Self):
        if ctx:
            return equal(env, ctx, ty1, ctx.instance())
        else:
            return tyinstance(ty1, Object)
    elif tyinstance(ty2, TypeVariable):
        return equal(env, ctx, ty1, env[ty2])
    elif tyinstance(ty1, TypeVariable):
        return equal(env, ctx, env[ty1], ty2)
    elif tyinstance(ty1, Base):
        return tyinstance(ty2, shallow(ty1))
    else:
        return False
    

def merge(ty1, ty2):
    if tyinstance(ty1, Dyn):
        return ty2
    elif tyinstance(ty2, Dyn):
        return Dyn
    elif tyinstance(ty1, Choice):
        return Choice(ty1.name, merge(selL(ty1.name, ty1), selL(ty1.name, ty2)), merge(selR(ty1.name, ty1), selR(ty1.name, ty2)))
    elif tyinstance(ty2, Choice):
        return Choice(ty2.name, merge(selL(ty2.name, ty1), selL(ty2.name, ty2)), merge(selR(ty2.name, ty1), selR(ty2.name, ty2)))
    elif tyinstance(ty1, List):
        if tyinstance(ty2, List):
            return List(merge(ty1.type, ty2.type))
        elif tyinstance(ty2, Tuple):
            return Tuple(*[merge(ty1.type, ty2m) for ty2m in ty2.elements])
        else: return ty1
    elif tyinstance(ty1, Dict):
        if tyinstance(ty2, Dict):
            return Dict(merge(ty1.keys, ty2.keys), merge(ty1.values, ty2.values))
        else: return ty1
    elif tyinstance(ty1, Tuple):
        if tyinstance(ty2, Tuple) and len(ty1.elements) == len(ty2.elements):
            return Tuple(*[merge(e1, e2) for e1, e2 in zip(ty1.elements, ty2.elements)])
        else: return ty1
    elif tyinstance(ty1, Object):
        if tyinstance(ty2, Object):
            nty = {}
            for n in ty1.members:
                if n in ty2.members:
                    nty[n] = merge(ty1.members[n],ty2.members[n])
                elif flags.MERGE_KEEPS_SOURCES: nty[n] = ty1.members[n]
            if not flags.CLOSED_CLASSES:
                for n in ty2.members:
                    if n not in nty:
                        nty[n] = ty2.members[n]
            return Object(ty1.name, nty)
        elif tyinstance(ty2, Function):
            if '__call__' in ty1.members:
                cty = merge(ty1.members['__call__'],ty2)
            else: cty = ty2
            return Object(ty1.name, {'__call__': ty2})
        else: return ty1
    elif tyinstance(ty1, Class):
        if tyinstance(ty2, Class):
            nty = {}
            for n in ty1.members:
                if n in ty2.members:
                    nty[n] = merge(ty1.members[n],ty2.members[n])
                elif flags.MERGE_KEEPS_SOURCES: nty[n] = ty1.members[n]
            if not flags.CLOSED_CLASSES:
                for n in ty2.members:
                    if n not in nty:
                        nty[n] = ty2.members[n]
            ity = {}
            for n in ty1.instance_members:
                if n in ty2.instance_members:
                    nty[n] = merge(ty1.instance_members[n],ty2.instance_members[n])
                elif flags.MERGE_KEEPS_SOURCES: nty[n] = ty1.instance_members[n]
            if not flags.CLOSED_CLASSES:
                for n in ty2.instance_members:
                    if n not in nty:
                        nty[n] = ty2.instance_members[n]
            return Class(ty1.name, nty, ity)
        else: return ty1
    elif tyinstance(ty1, Function):
        if tyinstance(ty2, Function):
            return Function(merge_params(ty1.froms, ty2.froms), merge(ty1.to, ty2.to))
        else: return ty1
    else: return ty1

def merge_params(p1, p2):
    if pinstance(p1, DynParameters):
        return p2
    elif pinstance(p2, DynParameters):
        return DynParameters
    else:
        args = p2.lenmatch(p1.parameters)
        if args == None:
            return p1
        elif pinstance(p1, AnonymousParameters):
            return AnonymousParameters([merge(t1, t2) for t1, t2 in args])
        elif pinstance(p1, NamedParameters):
            return NamedParameters([(k, merge(t1, t2)) for (k,t1), t2 in zip(p1.parameters, map(lambda x: x[1], args))])
        else: raise UnknownTypeError()

def occurs(t1, t2):
    if (tyinstance(t2, Function)):
        #check dom and cod
        occ = False
        if pinstance(t2.froms, DynParameters):
            return occ
        elif pinstance(t2.froms, AnonymousParameters):
            for t in t2.froms.parameters:
                occ = occ or occurs(t1, t)
            occ = occ or occurs(t1, t2.to)
            return occ
        elif pinstance(t2.froms, NamedParameters):
            for _,t in t2.froms.parameters:
                occ = occ or occurs(t1, t)
            occ = occ or occurs(t1, t2.to)
            return occ
        else:
            raise UnknownTypeError()    
    if (tyinstance(t2, Tuple)):
        #check components
        occ = False
        for e in t2.elements:
            occ = occ or occurs(t1, e)
        return occ            
    elif (tyinstance(t2, List)):
        #check underlying type
        underlying = t2.type
        return occurs(t1, underlying)
    elif (tyinstance(t2, Dict)):
        #check key and value types
        return occurs(t1, t2.keys) or occurs(t1, t2.values)
    elif (tyinstance(t2, Object)):
        #check types of attributes
        occ = False
        for n in t2.members:
            occ = occ or occurs(t1, n)
        return occ
    elif (tyinstance(t2, Class)):
        #check methods and attributes
        occ = False
        for n in t2.members:
            occ = occ or occurs(t1, n)
        return occ
    else:
        return t1 == t2

def hasDyn(t2):
    if (tyinstance(t2, Function)):
        #check dom and cod
        occ = False
        if pinstance(t2.froms, DynParameters):
            return occ
        elif pinstance(t2.froms, AnonymousParameters):
            for t in t2.froms.parameters:
                occ = occ or hasDyn(t)
            occ = occ or hasDyn(t2.to)
            return occ
        elif pinstance(t2.froms, NamedParameters):
            for _,t in t2.froms.parameters:
                occ = occ or hasDyn(t)
            occ = occ or hasDyn(t2.to)
            return occ
        else:
            raise UnknownTypeError()    
    if (tyinstance(t2, Tuple)):
        #check components
        occ = False
        for e in t2.elements:
            occ = occ or hasDyn(e)
        return occ            
    elif (tyinstance(t2, List)):
        #check underlying type
        underlying = t2.type
        return hasDyn(underlying)
    elif (tyinstance(t2, Dict)):
        #check key and value types
        return hasDyn(t2.keys) or hasDyn(t2.values)
    elif (tyinstance(t2, Object)):
        #check types of attributes
        occ = False
        for n in t2.members:
            occ = occ or hasDyn(n)
        return occ
    elif (tyinstance(t2, Class)):
        #check methods and attributes
        occ = False
        for n in t2.members:
            occ = occ or hasDyn(n)
        return occ
    elif (tyinstance(t2, Choice)):
        lty = selL(t2.name, t2)
        rty = selR(t2.name, t2)
        return hasDyn(lty) or hasDyn(rty)
    else:
        return t2 == Dyn

def hasTvar(t2):
    if (tyinstance(t2, Function)):
        #check dom and cod
        occ = False
        if pinstance(t2.froms, DynParameters):
            return occ
        elif pinstance(t2.froms, AnonymousParameters):
            for t in t2.froms.parameters:
                occ = occ or hasTvar(t)
            occ = occ or hasTvar(t2.to)
            return occ
        elif pinstance(t2.froms, NamedParameters):
            for _,t in t2.froms.parameters:
                occ = occ or hasTvar(t)
            occ = occ or hasTvar(t2.to)
            return occ
        else:
            raise UnknownTypeError()    
    if (tyinstance(t2, Tuple)):
        #check components
        occ = False
        for e in t2.elements:
            occ = occ or hasTvar(e)
        return occ            
    elif (tyinstance(t2, List)):
        #check underlying type
        underlying = t2.type
        return hasTvar(underlying)
    elif (tyinstance(t2, Dict)):
        #check key and value types
        return hasTvar(t2.keys) or hasTvar(t2.values)
    elif (tyinstance(t2, Object)):
        #check types of attributes
        occ = False
        for n in t2.members:
            occ = occ or hasTvar(n)
        return occ
    elif (tyinstance(t2, Class)):
        #check methods and attributes
        occ = False
        for n in t2.members:
            occ = occ or hasTvar(n)
        return occ
    elif (tyinstance(t2, Choice)):
        lty = selL(t2.name, t2)
        rty = selR(t2.name, t2)
        return hasTvar(lty) or hasTvar(rty)
    else:
        return tyinstance(t2, TypeVariable)

def patternMeet(p1, p2):
    if (tyinstance(p1, Choice)):
        left = patternMeet(selL(p1.name, p1), selL(p1.name, p2))
        right = patternMeet(selR(p1.name, p1), selR(p1.name, p2))
        return Choice(p1.name, left, right)
    elif (tyinstance(p2, Choice)):
        left = patternMeet(selL(p2.name, p1), selL(p2.name, p2))
        right = patternMeet(selR(p2.name, p1), selR(p2.name, p2))
        return Choice(p2.name, left, right)
    else:
        return p1 and p2

def domSub(sub):
    return sub.keys()

def mergeSubs(chc, sub1, sub2):
    combinedSub = list(sub1.keys()) + list(sub2.keys())
    #combDom = domSub(combinedSub)
    newSubs = {}
    for tv in combinedSub:
        if tv in sub1 and tv in sub2:
            newSubs[tv] = makeChoice(chc, sub1[tv], sub2[tv])
        elif tv in sub1 and tv not in sub2:
            newSubs[tv] = makeChoice(chc, sub1[tv], fresh_tvar())
        else:
            newSubs[tv] = makeChoice(chc, fresh_tvar(), sub2[tv])
    return newSubs

def replaceFreeSymbols(cost):
    if tyinstance(cost, Choice):
        #NOTE removing selL selR makeChoice...
        #return Choice(cost.name, replaceFreeSymbols(cost.left), replaceFreeSymbols(selR(cost.name, cost)))
        return Choice(cost.name, replaceFreeSymbols(selL(cost.name,cost)), replaceFreeSymbols(selR(cost.name, cost)))
    else:
        retCost = cost
        for c in cost.free_symbols:
            retCost = retCost.subs(c, 10000)
        return retCost

def findMinimumDepth(cost, pattern, maxCount):
    def findMin(cost, pattern, currentCount):
        if (pattern == False or currentCount > maxCount):
            return float("inf"), ""
        elif tyinstance(cost, Choice):
            if tyinstance(pattern, Choice):
                lcost, lpath = findMinimum(selL(cost.name, cost), selL(cost.name, pattern), currentCount + 1)
                rcost, rpath = findMinimum(selR(cost.name, cost), selR(cost.name,pattern), currentCount + 1)
            else:
                lcost, lpath = findMinimum(selL(cost.name, cost), selL(cost.name, pattern), currentCount + 1)
                rcost, rpath = findMinimum(selR(cost.name, cost), selR(cost.name, pattern), currentCount + 1)
            if (lcost < rcost):
                return lcost,  cost.name + ".1," + lpath
            else:
                return rcost,  cost.name + ".2," + rpath
        else:
            retCost = cost
            for c in cost.free_symbols:
                retCost = retCost.subs(c, 10000)
            return retCost, ""
        #return cost, ""
    return findMin(cost, pattern, 0)

def findMinimum(cost, pattern):
    if (pattern == False):
        return float("inf"), ""
    elif tyinstance(cost, Choice):
        if tyinstance(pattern, Choice):
            lcost, lpath = findMinimum(selL(cost.name, cost), selL(cost.name, pattern))
            rcost, rpath = findMinimum(selR(cost.name, cost), selR(cost.name,pattern))
        else:
            lcost, lpath = findMinimum(selL(cost.name, cost), selL(cost.name, pattern))
            rcost, rpath = findMinimum(selR(cost.name, cost), selR(cost.name, pattern))
        if (lcost < rcost):
            return lcost,  cost.name + ".1," + lpath
        else:
            return rcost,  cost.name + ".2," + rpath
    else:
        retCost = cost
        for c in cost.free_symbols:
            retCost = retCost.subs(c, 10000)
        return retCost, ""
        #return cost, "

def dyn_cost(cost):
    if tyinstance(cost, Choice):
            return dyn_cost(selL(cost.name, cost))
    else:        
        return cost

def static_cost(cost):
    if tyinstance(cost, Choice):
            return static_cost(selR(cost.name, cost))
    else:        
        return cost

def compare_costs(cost1, cost2, loopEnv):
    free1 = cost1.free_symbols
    free2 = cost2.free_symbols
    f1len = len(free1)
    f2len = len(free2)
    if (free1 == free2):
        if (replaceFreeSymbols(cost1) <= replaceFreeSymbols(cost2)):
            print("The two configurations have casts in the same loops but the dynamic configuration has less or cheaper casts")
        else:
            print("The two configurations have casts in the same loops but the static configuration has less or cheaper casts")
    elif (f1len <= f2len):
        for l in (set(free2) - set(free1)):
            print("The static configuration has a cast in the loop on line %s that isn't in the dynamic configuration " % str(loopEnv[l]))
    else:
        for l in (set(free1) - set(free2)):
            print("The dynamic configuration has a cast in the loop on line %s that isn't in the static configuration " % str(loopEnv[l]))
        
def codSub(sub):
    return sub.values()

def applySub(ty, sub):
    if tyinstance(ty, TypeVariable) and ty in sub:
        return sub[ty]
    elif tyinstance(ty, Function):
        if tyinstance(ty.froms, NamedParameters):
            newFroms = NamedParameters(list(map(lambda el: (el[0], applySub(el[1], sub)), ty.froms.parameters)))
            newTo = applySub(ty.to, sub)
            return reduceChoices(Function(newFroms, newTo))
        elif tyinstance(ty.froms, AnonymousParameters):
            newFroms = AnonymousParameters(list(map(lambda el: applySub(el, sub), ty.froms.parameters)))
            newTo = applySub(ty.to, sub)
            return reduceChoices(Function(newFroms, newTo))
        else:
            newFroms = ty.froms
            newTo = applySub(ty.to, sub)
            return reduceChoices(Function(newFroms, newTo))
    elif tyinstance(ty, Choice):
        return reduceChoices(makeChoice(ty.name, applySub(selL(ty.name,ty), sub), applySub(selR(ty.name,ty), sub)))
    elif (tyinstance(ty, List)):
        return reduceChoices(List(applySub(ty.type, sub)))
    elif (tyinstance(ty, Dict)):
        return reduceChoices(Dict(applySub(ty.keys, sub), applySub(ty.values, sub)))
    elif (tyinstance(ty, Tuple)):
        return reduceChoices(Tuple(*list(map(lambda el: applySub(el, sub), ty.elements))))
    else:
        return ty

def compose(sub1, sub2):
    newSub = sub1.copy()
    for i,v in sub2.items():
        sub2[i] = applySub(v, newSub)
        if i not in newSub:
            newSub[i] = sub2[i]        
    return newSub

def isPlain(ty):
    return tyinstance(ty, Int) or tyinstance(ty, Float) or tyinstance(ty, Bytes) or tyinstance(ty, String) or tyinstance(ty, Bool)

def reduceChoices(ty):
    if (tyinstance(ty, Choice)):
        left = reduceChoices(selL(ty.name, ty))
        right = reduceChoices(selR(ty.name, ty))
        return makeChoice(ty.name, left, right)
    elif (tyinstance(ty, List)):
         return List(reduceChoices(ty.type))
    elif tyinstance(ty, Tuple):
        return Tuple(*[reduceChoices(t) for t in ty.elements])
    elif tyinstance(ty, Dict):
        return Dict(reduceChoices(ty.keys), reduceChoices(ty.values))
    elif tyinstance(ty, Object):
        newTy = ty.copy()
        newTy.values = [reduceChoices(v) for v in newTy.values]
        return newTy            
    elif tyinstance(ty, Function):
        if tyinstance(ty.froms, NamedParameters):
            names = [n[0] for n in ty.froms.parameters]
            types = [t[1] for t in ty.froms.parameters]
            selTypes = map(reduceChoices, types)
            newPList = list(zip(names, selTypes))
            return Function(NamedParameters(newPList), reduceChoices(ty.to))
        elif tyinstance(ty.froms, AnonymousParameters):
            selTypes = map(reduceChoices, ty.froms.parameters)
            return Function(AnonymousParameters(list(selTypes)), reduceChoices(ty.to))
        else:
            return Function(ty.froms, reduceChoices(ty.to))
    else:
        return ty #not handling class for now

def liftToFunc(ty):
    if (tyinstance(ty, Function)):
        #if tyinstance(ty.froms, DynParameters):
        #    dom = DynParameters
        #elif tyinstance(ty.froms, AnonymousParameters):
        #    dom = ty.froms.parameters
        #else:
        #    dom = [x[1] for x in ty.froms.parameters]
        return ty.froms, ty.to, True, {}
    elif (tyinstance(ty, Choice)):
        ltydom, ltycod, lpat, subl = liftToFunc(selL(ty.name,ty))
        rtydom, rtycod, rpat, subr = liftToFunc(selR(ty.name, ty))                
        return makeChoice(ty.name, ltydom, rtydom), makeChoice(ty.name, ltycod, rtycod), makeChoice(ty.name, lpat, rpat), mergeSubs(ty.name, subl, subr)
    elif (tyinstance(ty, Dyn)):
        return DynParameters, Dyn, True, {}
    elif (tyinstance(ty, TypeVariable)):
        sub, pat, ty = unify(ty, Function(AnonymousParameters([fresh_tvar()]), fresh_tvar()))
        dom = ty.froms
        cod = ty.to
        return dom, cod, pat, sub
    else:
        return AnonymousParameters([Dyn]), Dyn, False, {}

def isSlicable(ty):
    if tyinstance(ty, Choice):
        left = isSlicable(selL(ty.name, ty))
        right = isSlicable(selR(ty.name, ty))
        return makeChoice(ty.name, left, right)
    else:
        return any(tyinstance(ty, kind) for kind in [List, Tuple, String, Bytes, Dyn, TypeVariable])

def get_ast_vars(node):
    if isinstance(node, ast.List):
        return [get_ast_vars(x) for x in node.elts]
    elif isinstance(node, ast.Tuple):
        return [get_ast_vars(x) for x in node.elts]
    elif isinstance(node, ast.Param):
        return [x]
def unify(t1, t2):
    if (t1 == Dyn):
        return {}, True, t2
    elif (t2 == Dyn):
        return {}, True, t1
    elif (tyinstance(t1, TypeVariable)):
        #sub t1 for t2 with occurs check
        if (t1 != t2 and occurs(t1, t2)):
            return {}, False, t1
        elif (hasDyn(t2)):
            if (tyinstance(t2, Function)):
                if (tyinstance(t2.froms, DynParameters)):
                    fVars = Function(AnonymousParameters([fresh_tvar()]), fresh_tvar())
                    (newSub, pat, ty) = unify(t1, fVars)
                    (secSub, secPat, resTy) =  unify(fVars, t2)
                    return (compose(secSub, newSub), patternMeet(secPat, pat), resTy)
                elif (tyinstance(t2.froms, Choice)):
                    lParams = selL(t2.froms.name, t2.froms)
                    rParams = selR(t2.froms.name, t2.froms)
                    lFunc = Function(lParams, t2.to)
                    rFunc = Function(rParams, t2.to)
                    return unify(makeChoice(t2.froms.name, t1, t1), makeChoice(t2.froms.name, lFunc, rFunc))
                else:
                    domList = [fresh_tvar() for _ in t2.froms.parameters]
                    fVars = Function(AnonymousParameters(domList), fresh_tvar())
                    (newSub, pat, ty) = unify(t1, fVars)
                    (secSub, secPat, resTy) =  unify(fVars, t2)
                    sub = compose(secSub, newSub)
                    return (sub, patternMeet(secPat, pat), applySub(sub, resTy))                        
            elif (tyinstance(t2, Choice)):
                #pdb.set_trace()
                name = t2.name
                newTy = Choice(name, t1, t1)
                return unify(newTy, t2)
            elif (tyinstance(t2, Object)):
                newSub = sub
                pat = True
                newObj = t2.copy()
                for field in t2.members:
                    newObj[field] = fresh_tvar()
                (sub, pat, ty) = unify(newObj, t2)
                sub[t1] = ty
                return (sub, pat, ty)
            elif (tyinstance(t2, List)):
                underlying = t2.type
                fv = fresh_tvar()
                (sub1, pat1, ty1) = unify(t1, List(fv))
                (newSub, newPat, ty2) = unify (fv, underlying)
                return (compose(newSub, sub1), patternMeet(pat1, newPat), List(ty2))
            elif (tyinstance(t2, Tuple)):
                tvarList = []
                for ty in t2.elements:
                    tvarList.append(fresh_tvar())
                tempType = Tuple(*tvarList)
                firstSub, firstPat, ty1 = unify(t1, tempType)
                newSub, newPat, ty2 =  unify(tempType, t2)
                return (compose(newSub, firstSub), patternMeet(newPat, firstPat), ty2)
            elif (tyinstance(t2, Dict)):
                key = fresh_tvar()
                val = fresh_tvar()
                sub1, pat1, ty1 = unify(t1, Dict(key, val))
                sub2, pat2, ty2 = unify(Dict(key, val), t2)
                return (compose(sub2, sub1), patternMeet(pat2, pat1), ty2)
            elif (tyinstance(t2, Class)):
                return {}, False, t1               
            else:
                return {}, False, t1               
            #elif (tyinstance(t2, list)):
            #    newSub = sub
            #    freshVars = 
            #    for ty in t2:
            #        newSub = unify(fresh_tvar() , ty, newSub)
            #    return newSub
        else:
            sub = {}
            sub[t1] =  t2
            pat = True
            return (sub, pat, t2)
    elif (tyinstance(t2, TypeVariable)):
        return unify(t2, t1)
    elif (tyinstance(t1, Function) and tyinstance(t2, Function)):
        if (tyinstance(t1.froms, DynParameters)): 
            pat1 = True
            sub, pat2, codty = unify(t1.to, t2.to)
            return sub, pat2, Function(t2.froms, codty)
        elif tyinstance(t2.froms, DynParameters):
            pat1 = True
            sub, pat2, codty = unify(t1.to, t2.to)
            return sub, pat2, Function(t1.froms, codty)
        elif (tyinstance(t1.froms, NamedParameters)):
            params = t1.froms.parameters
            paramNames = [x[0] for x in params]
            types1 = [x[1] for x in params]
            if (tyinstance(t2.froms, NamedParameters)):
                otherParams = t2.froms.parameters
                names2 = [x[0] for x in otherParams]
                types2 = [x[1] for x in otherParams]
                zippedTypes = zip(types1, types2)
                pat = True
                joinedDom = []
                sub = {}
                for ty1, ty2 in zippedTypes:
                    sub1, pat1, ty = unify(applySub(ty1, sub), applySub(ty2, sub))
                    sub = compose(sub, sub1)
                    pat = patternMeet(pat, pat1)
                    joinedDom.append(ty)
                zippedParams = list(zip(paramNames, joinedDom))
                codSub, codPat, codTy = unify(t1.to, t2.to)                                
                return compose(codSub, sub), patternMeet(codPat, pat), Function(NamedParameters(zippedParams), codTy)
            elif (tyinstance(t2.froms, Choice)):
                    lParams = selL(t2.froms.name, t2.froms)
                    rParams = selR(t2.froms.name, t2.froms)
                    lFunc = Function(lParams, t2.to)
                    rFunc = Function(rParams, t2.to)
                    return unify(makeChoice(t2.froms.name, t1, t1), makeChoice(t2.froms.name, lFunc, rFunc))
            else: #Anonymous parameters
                paramTypes = t2.froms.parameters
                zippedTypes = zip(types1, paramTypes)
                joinedDom = []
                sub = {}
                pat = True
                for ty1, ty2 in zippedTypes:
                    sub1, pat1, ty = unify(applySub(ty1, sub), applySub(ty2, sub))
                    sub = compose(sub, sub1)
                    pat = patternMeet(pat, pat1)
                    joinedDom.append(ty)
                codSub, codPat, codTy = unify(t1.to, t2.to)
                zippedParams = list(zip(paramNames, joinedDom))
                return compose(codSub, sub), patternMeet(codPat, pat), Function(NamedParameters(zippedParams), codTy)
                #t2 Anonymous params
        elif (tyinstance(t1.froms, Choice)):
            lParams = selL(t1.froms.name, t1.froms)
            rParams = selR(t1.froms.name, t1.froms)
            lFunc = Function(lParams, t1.to)
            rFunc = Function(rParams, t1.to)
            return unify(makeChoice(t1.froms.name, lFunc, rFunc), makeChoice(t1.froms.name, t2, t2))    
        else:
            types1 = t1.froms.parameters
            if (tyinstance(t2.froms, NamedParameters)):
                otherParams = t2.froms.parameters
                namedParams = [x[0] for x in otherParams]
                types2 = [x[1] for x in otherParams]
                zippedTypes = zip(types1, types2)
                pat = True
                joinedDom = []
                sub = {}
                for ty1, ty2 in zippedTypes:
                    sub1, pat1, ty = unify(applySub(ty1, sub), applySub(ty2, sub))
                    sub = compose(sub, sub1)
                    pat = patternMeet(pat, pat1)
                    joinedDom.append(ty)
                codSub, codPat, codTy = unify(t1.to, t2.to)
                zippedParams = list(zip(namedParams, joinedDom))
                return compose(codSub, sub), patternMeet(codPat, pat), Function(NamedParameters(zippedParams), codTy)
            else:
                paramTypes = t2.froms.parameters
                zippedTypes = zip(types1, paramTypes)
                joinedDom = []
                sub = {}
                pat = True
                for ty1, ty2 in zippedTypes:
                    sub1, pat1, ty = unify(applySub(ty1, sub), applySub(ty2, sub))
                    sub = compose(sub, sub1)
                    pat = patternMeet(pat, pat1)
                    joinedDom.append(ty)
                codSub, codPat, codTy = unify(t1.to, t2.to)                                
                return compose(codSub, sub), patternMeet(codPat, pat), Function(AnonymousParameters(joinedDom), codTy)
            #t1 Anonymous params
    elif (tyinstance(t1, Tuple) and tyinstance(t2, Tuple)):
        zippedTypes = zip(t1.elements, t2.elements)
        pat = True
        tupList = []
        for ty1, ty2 in zippedTypes:
            sub, pat2, ty = unify(ty1, ty2)
            pat = patternMeet(pat, pat2)
            tupList.append(ty)
        return sub, pat, Tuple(*tupList)
    elif (tyinstance(t1, List) and tyinstance(t2, List)):
        sub, pat, ty = unify (t1.type, t2.type)
        return sub, pat, List(ty)
    elif (tyinstance(t1, Dict) and tyinstance(t2, Dict)):
        sub1, pat1, ty1 = unify (t1.keys, t2.keys)
        sub2, pat2, ty2 = unify (t1.values, t2.values)
        sub = compose(sub2, sub1)
        pat = patternMeet(pat2, pat1)
        return sub, pat, Dict(ty1,ty2)
    elif (tyinstance(t1, Choice)):
        if (tyinstance(t2, Choice) and t2.name == t1.name):
            (sub1, pat1, tl) = unify(selL(t1.name, t1), selL(t2.name, t2))
            (sub2, pat2, tr) = unify(selR(t1.name, t1), selR(t1.name, t2))
            pat = makeChoice(t1.name, pat1, pat2)
            sub = mergeSubs(t1.name, sub1, sub2)
            return sub, pat, makeChoice(t1.name, tl, tr)
        else:
            left = selL(t1.name, t2)
            right = selR(t1.name, t2)
            lifted = Choice(t1.name, left, right)
            return unify(t1, lifted)
    elif (tyinstance(t2, Choice)):
        return unify(t2, t1)
    elif  isPlain(t1) and isPlain(t2):
        if t1 == t2:
            return {}, True, t1
        else:
            return {}, False, t1
    else:
        return {}, False, Dyn
            
            
        
                    



