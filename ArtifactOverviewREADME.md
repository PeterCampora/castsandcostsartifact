# Getting Started
# About

This is the artifact accompanying the paper, *Casts and Costs: Harmonizing
Safety and Performance in Gradual Typing*, by John Peter Campora III, Sheng
Chen, Eric Walkingshaw, to appear at
[ICFP 2018](https://conf.researchr.org/home/icfp-2018).

The artifact consists of:

1. Our tool *Herder* for performing variational analyses of gradually typed
   Python programs.

2. The suite of benchmarks used in the paper.

3. Log files showing the outputs of the analyses, used to generate the figures
   in the paper.


# Installing Herder

Herder is implemented as modified version of
[Reticulated Python](https://github.com/mvitousek/reticulated).

Since the tool has few prerequisites, we have provided it as source code rather
than as a virtual machine machine image. Running the artifact requires:

1. A Unix shell
2. Python 3.5 (tested with 3.5.2 and 3.5.5)
3. Two Python packages: `sympy` and `perf`

One minor complication is that Reticulated Python works only with Python
versions 3.2--3.5, but not with the latest version 3.6.

Python 3.5 is the default version of Python 3 on Ubuntu Linux.

On other systems, a convenient solution for managing multiple Python
installations is the [`pyenv`](https://github.com/pyenv/pyenv) tool.

We provide installation instructions below for both starting from scratch in
Ubuntu, and starting from any other system with a working installation of
`pyenv`. However, we don't provide instructions for installing `pyenv` itself.


## Installation instructions (Ubuntu)

1. Install Python 3.5.

   ```
   sudo apt-get install python3
   ```

2. Install the Python package manager, `pip3`.

   ```
   sudo apt-get install python3-pip
   ```

3. Install the Python packages `sympy` and `perf`.

   ```
   sudo pip3 install sympy perf
   ```

4. Install Herder. From the root of this repository:

   ```
   cd Herder/reticulated
   sudo python3 setup.py install
   ```


## Installation instructions (pyenv)

1. Install Python 3.5 and make it the current version.

   ```
   pyenv install 3.5.5
   pyenv global 3.5.5
   ```

2. Install the Python packages `sympy` and `perf`.

   ```
   pip3 install --user sympy perf
   ```

3. Install Herder. From the root of this repository:

   ```
   cd Herder/reticulated
   python3 setup.py install
   pyenv rehash

   ```


# Running Herder on the Benchmarks

The benchmarks used in the paper are located in `tests/benchmarks`. The
benchmark names align with those used in the paper, except that sm(MC) is
`monte_carlo.py`, sm(FFT), is `fft.py`, and sm(SOR) is `sci_markSOR.py` (also
called simply "sci_mark" elsewhere).

To run Herder's analysis, run Reticulated Python with the `--guarded` flag.

For example, to run Herder on the sm(FFT) benchmark, run:

```
retic --guarded fft.py
```

This will produce several lines of output:

 * The first line of output is prototype support for S4, described in the
   paper, which qualitatively compares the costs of the fully dynamic and the
   most-static configurations. For sm(FFT), it reports that the configurations
   have similar costs due to casts in the same loops.

 * The next two lines of the output describe the number of choices created and
   the complete variational cost. This information is not very useful to
   end-users, but is important for linking Herder's analysis to the theory and
   results described in the paper. Note that the variational cost has choices
   with names like `C1` and loop labels with names like `L1`.

 * The next several lines list Herder's recommended type annotations for each
   function in the benchmark. Note that some functions' recommended types might
   contain a variational type (such as `C1<T1,T2>`). This indicates that a
   change in the annotation does not cause a change in performance. The more
   static type is in the right alternative of the choice, so a user wanting to
   increase type safety should pick the right alternative.

 * The second-to-last line states the number of function parameters that Herder
   suggests "statifying" (i.e. adding static type annotations for). This is
   derived from the recommended type annotations above. Note that the number of
   recommended static types for the synthetic benchmarks is sometimes smaller
   and more accurate than in the paper as originally submitted. This is due to
   a bug in the original implementation that has since been fixed. The revised
   version of the paper will contain the corrected numbers.

 * Finally, the last line reports the time taken to perform the analysis.

You can run the analysis on all of the bencmarks at once by running the
`herderInfo.sh` script, but this will likely be too much information to
digest at once.

# Step by Step
# Results for Figure 10

Figure 10 consists of essentially four parts:

1. Basic information about each of the benchmarks, which can be obtained by
   observing the benchmark files in `tests/benchmarks`. This information is in
   columns 1--3 of the table in Figure 10.

2. The outcome of the variational analysis performed by Herder, which can be
   obtained by running Herder on each of the benchmark files in
   `tests/benchmarks`, as described above. This information is in columns 4--5
   of the table.

3. Several timing results obtained by executing the program configuration
   recommended by Herder (column 6), by executing the fully dynamic
   configuration (columns 7--8), and by executing all other potential
   configurations of the original benchmark, or a sample of 100 potential
   configurations for larger examples (columns 9--12).

4. A column indicating whether Herder's recommendation was among the top-3
   fastest configurations (column 13).


## Submitted Data

Reproducing the timing results from the original benchmarks will take several
hours on most machines. Therefore, we have also provided both the generated
configurations and our timing data in the artifact submission. Our timing data
was computed on a System76 Galago Pro with Ubuntu 16.04.

The time to execute each of the original benchmarks in Reticulated Python (that
is, the runtime of the fully dynamic configurations) are stored in
`tests/benchmarks/log.txt`. All other configurations and timing results are
stored in the directory `tests/Fig10_Benches`.

Each subdirectory in `tests/Fig10_Benches` corresponds to one of the original
benchmarks in `tests/benchmarks`. Each subdirectory contains:

 * Several numbered Reticulated Python files corresponding to the sample of the
   configuration space that we timed. For smaller examples, this will be all
   possible configurations of the original benchmark, for larger examples, this
   will be a sample of 100 configurations of the original file, obtained by
   running the `generateBenches.sh` script (see below).
 
 * The specific configuration recommended by Herder, in a file named
   `pred_{benchmark-name}.py`.
 
 * A `log.txt` file containing the runtime of executing all configurations
   (both the sample and Herder's recommendation) in Reticulated Python.

The last column of the table, which summarizes how Herder's recommendation
compared to the sample, can be obtained by executing `results.sh` from the
directory `tests/Fig10_Benches`.

You can also generate the ratio data from several scripts.
The Dynamic Ratio column can be obtained by executing `python3 DynRatios.py`
and similarly the Best Ratio column and Worst Ratio columns are generated from
`python3 BestRatios.py` and `python3 WorstRatios.py`. The `BestRatios` script
only considers the sampled configurations in the individual benchmark folders
and not the dynamic configuration. Thus, if the Dynamic configuration is best, then the
output `DynRatios` would correspond to the Best Ratio in figure 10 instead of the ratio
reported by `BestRatios`.


## Recomputing the Timing Data

Recomputing the timing results requires an unmodified version of Reticulated
Python. Unfortunately, because of the way Herder is currently implemented, only
one of Herder or Reticulated Python can be installed at a time.

If you followed the installation instructions above, you should have Herder
installed. To install the unmodified version of Reticulated Python, simply
perform the last step of the installation instructions in the subdirectory
`reticulated` (instead of `Herder/reticulated`). To reinstall Herder, perform
the last step again in `Herder/reticulated`.

To re-time the original benchmarks (the fully dynamic configurations), navigate
to `tests/benchmarks` and run the `runBenches.sh` script.

To re-time the various other configurations of a benchmark, navigate to
`tests/Fig10_Benches/{benchmark-name}` and similarly run the `runBenches.sh`
script.

To re-time the configurations of *all* benchmarks, navigate to
`tests/Fig10_Benches` and run the `runAll.sh` script. Note that this may take
several hours.

Each of the above scripts will overwrite the corresponding `log.txt` files with
new timing results. The `sortTimes.py` script in each of the benchmark
subdirectories can be used to process these results, checking that Herder's
recommended configuration is among the top-3.


## Recomputing the Configurations

If you want to generate a new sample of random configurations for the larger
benchmarks, you can navigate to the corresponding benchmarks sub-directory at
`tests/Fig10_Benches/{benchmark-name}` and run the `generateBenches.sh` script.
This will sample 100 new configurations. Then you can use the `runBenches.sh`
script to re-time the configurations and the results will be stored in
`log.txt`.


# Results for Figure 11 

Figure 11 shows the time to perform Herder's analysis compared to the time to
perform an equivalent brute-force analysis, and compared to the time to
type-check and compute the cost of a single, fully dynamic configuration. It
shows how these times evolve as both the size of a program increases, measured
by lines of code (LOC), and as the size of its configuration space increases,
measured by the number of parameters that may be annotated or not.


## Submitted Data

The synthetically generated benchmarks, their configurations, and the timing
results are included in the directory `tests/Fig11_Benches`. Each subdirectory
corresponds to a row in the table in Figure 11.

For rows that contain an entry for the brute-force approach, the corresponding
subdirectory contains all of the configurations of the program (as numbered
`.py` files) and a `log.txt` file containing the timing result for analyzing
each configuration. For rows that do not contain an entry for the brute-force
approach (because the configuration space is too large), the subdirectory
contains only the fully dynamic example program.

The timing results for Herder's analysis are included in each subdirectory
of `tests/Fig11_Benches` as `log-herder.txt`.

The timing results for type checking and computing the cost of the all-dynamic
configurations are included in the `log-dynamic.txt` in the same subdirectories
as above.

In the `tests/Fig11_Benches` directory there are several scripts for viewing
the different columns in the timing data.
You can view the results for Herder by running `HerderTimes.sh` and the results
for computing the cost of the all-dynamic configurations with `DynamicTimes.sh`.
The total time taken by brute-force analysis can be computed from the `log.txt`
files in the various subdirectories by running  `bruteTime.sh`.



## Recomputing the Timing Data

To recompute the time to perform Herder's analysis on each generated benchmark,
ensure that Herder is installed (see above), navigate to `tests/Fig11_Benches`,
and execute the `runHerder.sh` script. This script simply runs Herder on the
fully dynamic configuration of each benchmark. You can view the new results
by running the `HerderTimes.sh` script again.

To recompute the other timing data, you must first install an alternative
version of Herder that does not perform variational analysis. Do this by
performing the last step of the installation instructions from the directory
`HerderNoVari/reticulated` (instead of `Herder/reticulated`). This will replace
the installation of Herder (or plain Reticulated Python) with this modified
version.

Now you can recompute the brute-force analysis for all of the benchmarks by
navigating to `tests/Fig11_Benches` and running the script `runBrute.sh`.
This will take several hours.

Once the brute-force analyses have finished, you can recompute the total time
of these analyses from the log files by running the `bruteTime.sh` script.

To recompute the timing results for type-checking and computing the cost of the
all-dynamic configurations, make sure that the non-variational version of
Herder is installed, then run the script `runDynamic.sh` from the
`tests/Fig11_Benches` directory.

Once this analysis has finished, you can view the newly generated results
by running the `DynamicTimes.sh` script again.


# New S3 Benchmarks

As part of our revision, we have performed a new evaluation of scenario S3. The
results of this evaluation are described and summarized in the file
`tests/S3Benches/results.txt`.

This evaluation is performed on 45 randomly chosen configurations from the nine
non-synthetic benchmarks in `tests/benchmarks`. The chosen configurations are
organized into subdirectories according to the parent benchmark, together with
both the original fully dynamic configuration of the benchmark and a `log.txt`
that contains the timing data for each of the configurations.

To print the timing results all configurations, run the script `printTimes.sh`
from the `tests/S3Benches` directory. The "pred" files in the output correspond
to configurations Herder finds that should have a lower cost than the randomly
generated corresponding configuration with the same index # above it.
For example, "pred11.py" corresponds to "fft11.py" in the first batch of output.

To recompute the timing data, first ensure that plain Reticulated Python is
installed by performing the last step of the installation instructions from the
`reticulated` directory. Then, run the script `runAll.sh` from the
`tests/S3Benches` to recompute the timing results for the original benchmark
and the randomly selected configurations. Finally, run the script `runPreds.sh`
to recompute the timing results for Herder's picks.

Unfortunately, there isn't an easy way to automate detecting whether any
changes in the results occur, but the timing data should not change too
drastically. Consequently, verifying that the same random configurations are
slower than the dynamic configurations can be done manually. If the set of
configurations all remain slower than the dynamic time, then examining the
prediction times versus the dynamic times can be used to see if Herder's
recommendations (which remain the same) retain their current correctness.


# Disclaimers

Right now the implementation of Herder is very much a research prototype.
Consequently, it works with a narrow subset of Python 3 programs, and it will
likely break on new benchmarks. Moreover, the current set of benchmarks are
modified subsets of the actual python benchmark suite programs that work well
with Herder's currently implemented features. Occasionally, bugs in the type
inference implementation will cause it to infer unsound types for programs.
Sometimes this happens when running Herder on certain programs like fft and
raytrace. We plan to iron out such bugs and support more Python features in the
future, and we also plan to make Herder even more scalable and accurate.
