import random

programHead = """
from six.moves import xrange
from itertools import islice
import time
import random
from array import array
import math



__contact__ = "collinwinter@google.com (Collin Winter)"
DEFAULT_ITERATIONS = 10000
DEFAULT_REFERENCE = 'sun'

"""

combinations_headers = ["def combinations(l):",
                        "def combinations(l)->List(Dyn):",
                        "def combinations(l:List(Dyn))->List(Dyn):"]

combinations_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


PI = 3.14159265358979323
SOLAR_MASS = 4 * PI * PI
DAYS_PER_YEAR = 365.24
#pdb.set_trace()
BODIES = {
    'sun': ([0.0, 0.0, 0.0], [0.0, 0.0, 0.0], SOLAR_MASS),

    'jupiter': ([4.84143144246472090e+00,
                 -1.16032004402742839e+00,
                 -1.03622044471123109e-01],
                [1.66007664274403694e-03 * DAYS_PER_YEAR,
                 7.69901118419740425e-03 * DAYS_PER_YEAR,
                 -6.90460016972063023e-05 * DAYS_PER_YEAR],
                9.54791938424326609e-04 * SOLAR_MASS),

    'saturn': ([8.34336671824457987e+00,
                4.12479856412430479e+00,
                -4.03523417114321381e-01],
               [(-2.76742510726862411e-03 * DAYS_PER_YEAR),
                (4.99852801234917238e-03 * DAYS_PER_YEAR),
                (2.30417297573763929e-05 * DAYS_PER_YEAR)],
               (2.85885980666130812e-04 * SOLAR_MASS)),

    'uranus': ([1.28943695621391310e+01,
                -1.51111514016986312e+01,
                -2.23307578892655734e-01],
               [2.96460137564761618e-03 * DAYS_PER_YEAR,
                2.37847173959480950e-03 * DAYS_PER_YEAR,
                -2.96589568540237556e-05 * DAYS_PER_YEAR],
               4.36624404335156298e-05 * SOLAR_MASS),

    'neptune': ([1.53796971148509165e+01,
                 -2.59193146099879641e+01,
                 1.79258772950371181e-01],
                [2.68067772490389322e-03 * DAYS_PER_YEAR,
                 1.62824170038242295e-03 * DAYS_PER_YEAR,
                 -9.51592254519715870e-05 * DAYS_PER_YEAR],
                5.15138902046611451e-05 * SOLAR_MASS)}


SYSTEM = list(BODIES.values())
PAIRS = combinations(SYSTEM)

"""

advance_headers = ["def advance(dt, n, bodies, pairs):",
                   "def advance(dt:float, n, bodies, pairs):",
                   "def advance(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy_headers = ["def report_energy(bodies, pairs, e):",
                         "def report_energy(bodies, pairs, e:float)->float:",
                         "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum_headers = ["def offset_momentum(ref, bodies, px, py, pz):",
                          "def offset_momentum(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest = """
def bench_nbody(loops, reference, iterations):
    # Set up global state
    offset_momentum(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy(SYSTEM, PAIRS, 0.0)
    return None

def main():
    bench_nbody(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

#bench_nbody(1,'sun',20000)
#t0 = time.time()
#main()
#t1 = time.time()
#print(t1-t0)
"""
combinations1_headers = ["def combinations1(l):",
                        "def combinations1(l)->List(Dyn):",
                        "def combinations1(l:List(Dyn))->List(Dyn):"]

combinations1_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


"""

advance1_headers = ["def advance1(dt, n, bodies, pairs):",
                   "def advance1(dt:float, n, bodies, pairs):",
                   "def advance1(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance1(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance1(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance1(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance1(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance1(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance1_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy1_headers = ["def report_energy1(bodies, pairs, e):",
                         "def report_energy1(bodies, pairs, e:float)->float:",
                         "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy1(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy1(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy1_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum1_headers = ["def offset_momentum1(ref, bodies, px, py, pz):",
                          "def offset_momentum1(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum1(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum1_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest1 = """
def bench_nbody1(loops, reference, iterations):
    # Set up global state
    offset_momentum1(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy1(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy1(SYSTEM, PAIRS, 0.0)
    return None

def main1():
    bench_nbody1(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

"""
combinations2_headers = ["def combinations2(l):",
                        "def combinations2(l)->List(Dyn):",
                        "def combinations2(l:List(Dyn))->List(Dyn):"]

combinations2_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


"""

advance2_headers = ["def advance2(dt, n, bodies, pairs):",
                   "def advance2(dt:float, n, bodies, pairs):",
                   "def advance2(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance2(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance2(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance2(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance2(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance2(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance2_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy2_headers = ["def report_energy2(bodies, pairs, e):",
                         "def report_energy2(bodies, pairs, e:float)->float:",
                         "def report_energy2(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy2(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy2(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy2(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy2(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy2(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy2(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy2_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum2_headers = ["def offset_momentum2(ref, bodies, px, py, pz):",
                          "def offset_momentum2(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum2(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum2(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum2(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum2(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum2(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum2_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest2 = """
def bench_nbody2(loops, reference, iterations):
    # Set up global state
    offset_momentum2(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy2(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy2(SYSTEM, PAIRS, 0.0)
    return None

def main2():
    bench_nbody2(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

"""

combinations3_headers = ["def combinations3(l):",
                        "def combinations3(l)->List(Dyn):",
                        "def combinations3(l:List(Dyn))->List(Dyn):"]

combinations3_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


"""

advance3_headers = ["def advance3(dt, n, bodies, pairs):",
                   "def advance3(dt:float, n, bodies, pairs):",
                   "def advance3(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance3(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance3(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance3(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance3(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance3(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance3_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy3_headers = ["def report_energy3(bodies, pairs, e):",
                         "def report_energy3(bodies, pairs, e:float)->float:",
                         "def report_energy3(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy3(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy3(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy3(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy3(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy3(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy3(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy3_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum3_headers = ["def offset_momentum3(ref, bodies, px, py, pz):",
                          "def offset_momentum3(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum3(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum3(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum3(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum3(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum3(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum3_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest3 = """
def bench_nbody3(loops, reference, iterations):
    # Set up global state
    offset_momentum3(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy3(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy3(SYSTEM, PAIRS, 0.0)
    return None

def main3():
    bench_nbody3(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

"""

combinations4_headers = ["def combinations4(l):",
                        "def combinations4(l)->List(Dyn):",
                        "def combinations4(l:List(Dyn))->List(Dyn):"]

combinations4_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


"""

advance4_headers = ["def advance4(dt, n, bodies, pairs):",
                   "def advance4(dt:float, n, bodies, pairs):",
                   "def advance4(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance4(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance4(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance4(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance4(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance4(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance4_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy4_headers = ["def report_energy4(bodies, pairs, e):",
                         "def report_energy4(bodies, pairs, e:float)->float:",
                         "def report_energy4(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy4(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy4(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy4(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy4(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy4(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy4(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy4_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum4_headers = ["def offset_momentum4(ref, bodies, px, py, pz):",
                          "def offset_momentum4(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum4(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum4(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum4(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum4(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum4(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum4_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest4 = """
def bench_nbody4(loops, reference, iterations):
    # Set up global state
    offset_momentum4(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy4(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy4(SYSTEM, PAIRS, 0.0)
    return None

def main4():
    bench_nbody4(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

"""

combinations5_headers = ["def combinations5(l):",
                        "def combinations5(l)->List(Dyn):",
                        "def combinations5(l:List(Dyn))->List(Dyn):"]

combinations5_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


"""

advance5_headers = ["def advance5(dt, n, bodies, pairs):",
                   "def advance5(dt:float, n, bodies, pairs):",
                   "def advance5(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance5(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance5(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance5(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance5(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance5(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance5_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy5_headers = ["def report_energy5(bodies, pairs, e):",
                         "def report_energy5(bodies, pairs, e:float)->float:",
                         "def report_energy5(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy5(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy5(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy5(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy5(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy5(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy5(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy5_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum5_headers = ["def offset_momentum5(ref, bodies, px, py, pz):",
                          "def offset_momentum5(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum5(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum5(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum5(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum5(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum5(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum5_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest5 = """
def bench_nbody5(loops, reference, iterations):
    # Set up global state
    offset_momentum5(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy5(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy5(SYSTEM, PAIRS, 0.0)
    return None

def main5():
    bench_nbody5(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

"""

int_log_2_headers = ["def int_log2(n):", "def int_log2(n:float):"]
int_log_2_body = """
    k = 1
    log = 0
    while k < (n+0.0):
        k *= 2
        log += 1
    if n != 1 << log:
        raise Exception("FFT: Data length is not a power of 2: %s" % n)
    return (log)

"""

FFT_num_flops_headers = ["def FFT_num_flops(N)->float:"
                         ,"def FFT_num_flops(N:float)->float:"]
FFT_num_flops_body = """
    return (5.0 * N - 2) * int_log2(N) + 2 * (N + 1)

"""
FFT_transform_internal_headers = ["def FFT_transform_internal(N, data, direction):",
                                  "def FFT_transform_internal(N:int, data, direction):",
                                  "def FFT_transform_internal(N:int, data, direction:int):"]

FFT_transform_internal_body = """
    n = int(N // 2)
    bit = 0
    dual = 1
    if n == 1:
        return None

    logn = int_log2(n)
    if N == 0:
        return None
    FFT_bitreverse(N, data)

    # apply fft recursion
    # this loop executed int_log2(N) times
    bit = 0
    while bit < logn:
        w_real = 1.0
        w_imag = 0.0
        theta = 2.0 * direction * math.pi / (2.0 * float(dual))
        s = math.sin(theta)
        t = math.sin(theta / 2.0)
        s2 = 2.0 * t * t
        for b in range(0, n, 2 * dual):
            i = 2 * b
            j = 2 * (b + dual)
            wd_real = data[j]
            wd_imag = data[j + 1]
            data[j] = data[i] - wd_real
            data[j + 1] = data[i + 1] - wd_imag
            data[i] += wd_real
            data[i + 1] += wd_imag
        for a in xrange(1, dual):
            tmp_real = w_real - s * w_imag - s2 * w_real
            tmp_imag = w_imag + s * w_real - s2 * w_imag
            w_real = tmp_real
            w_imag = tmp_imag
            for b in range(0, n, 2 * dual):
                i = 2 * (b + a)
                j = 2 * (b + a + dual)
                z1_real = data[j]
                z1_imag = data[j + 1]
                wd_real = w_real * z1_real - w_imag * z1_imag
                wd_imag = w_real * z1_imag + w_imag * z1_real
                data[j] = data[i] - wd_real
                data[j + 1] = data[i + 1] - wd_imag
                data[i] += wd_real
                data[i + 1] += wd_imag
        bit += 1
        dual *= 2
        return None

"""
FFT_bitreverse_headers = ["def FFT_bitreverse(N, data):"
                          ,"def FFT_bitreverse(N:int, data):"]

FFT_bitreverse_body = """
    n = int(N // 2)
    nm1 = n - 1
    j = 0
    for i in range(nm1):
        ii = i << 1
        jj = j << 1
        k = n >> 1
        if i < j:
            tmp_real = data[ii]
            tmp_imag = data[ii + 1]
            data[ii] = data[jj]
            data[ii + 1] = data[jj + 1]
            data[jj] = tmp_real
            data[jj + 1] = tmp_imag
        while k <= j:
            j -= k
            k >>= 1
        j += k
        return None

"""

FFT_transform_headers = ["def FFT_transform(N, data):"
, "def FFT_transform(N:int, data):"]

FFT_transform_body = """
    FFT_transform_internal(N, data, -1)
    return None

"""

FFT_inverse_headers = ["def FFT_inverse(N, data):"
                       ,"def FFT_inverse(N:int, data):"
                       ,"def FFT_inverse(N, data:float):"]
FFT_inverse_body = """
    n = N / 2
    norm = 0.0
    FFT_transform_internal(N, data, +1)
    norm = 1 / float(n)
    for i in xrange(N):
        data[i] *= norm
    return None

"""

restFFT = """
def bench_FFT(loops, N, cycles):
    twoN = 2 * N
    init_vec = array('d', [random.random() for i in range(twoN)]) #Random(7).RandomVector(twoN)
    range_it = xrange(loops)
    #t0 = perf.perf_counter()

    for _ in range_it:
        x = array('d')
        x[:] = init_vec[:]
        #x = copy_vector(init_vec)
        for i in xrange(cycles):
            FFT_transform(twoN, x)
            FFT_inverse(twoN, x)
            
    return None
    #return perf.perf_counter() - t0

"""

final = """
def main():
    t0 = time.time()
    twoN = 2 * 1024
    init_vec = array('d', [random.random() for i in range(twoN)]) #Random(7).RandomVector(twoN)
    for i in range(1000):
        FFT_bitreverse(twoN, init_vec)
        offset_momentum(BODIES[DEFAULT_REFERENCE], SYSTEM, 0.0, 0.0, 0.0)

    #bench_nbody5(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)
    #bench_FFT(10, 1024, 50)
    t1 = time.time()
    print(t1-t0)
    return None

main()
"""

indexList = []
i = 1
for i in range(100):
    prog = programHead
    combinations_index = random.randrange(0, len(combinations_headers))
    advance_index = random.randrange(0, len(advance_headers))
    report_energy_index = random.randrange(0, len(report_energy_headers))
    offset_momentum_index = random.randrange(0,len(offset_momentum_headers))    
    combinations = combinations_headers[combinations_index]  + combinations_body
    advance = advance_headers[advance_index]  + advance_body
    report_energy = report_energy_headers[report_energy_index]  + report_energy_body
    offset_momentum = offset_momentum_headers[offset_momentum_index]  + offset_momentum_body
    program =  combinations + advance + report_energy + offset_momentum
    prog += program
    for k in range(10):


        combinations1_index = random.randrange(0, len(combinations1_headers))
        advance1_index = random.randrange(0, len(advance1_headers))
        report_energy1_index = random.randrange(0, len(report_energy1_headers))
        offset_momentum1_index = random.randrange(0,len(offset_momentum1_headers))    
        combinations1 = combinations1_headers[combinations1_index]  + combinations1_body
        advance1 = advance1_headers[advance1_index]  + advance1_body
        report_energy1 = report_energy1_headers[report_energy1_index]  + report_energy1_body
        offset_momentum1 = offset_momentum1_headers[offset_momentum1_index]  + offset_momentum1_body

        combinations2_index = random.randrange(0, len(combinations2_headers))
        advance2_index = random.randrange(0, len(advance2_headers))
        report_energy2_index = random.randrange(0, len(report_energy2_headers))
        offset_momentum2_index = random.randrange(0,len(offset_momentum2_headers))    
        combinations2 = combinations2_headers[combinations2_index]  + combinations2_body
        advance2 = advance2_headers[advance2_index]  + advance2_body
        report_energy2 = report_energy2_headers[report_energy2_index]  + report_energy2_body
        offset_momentum2 = offset_momentum2_headers[offset_momentum2_index]  + offset_momentum2_body                

        combinations3_index = random.randrange(0, len(combinations3_headers))
        advance3_index = random.randrange(0, len(advance3_headers))
        report_energy3_index = random.randrange(0, len(report_energy3_headers))
        offset_momentum3_index = random.randrange(0,len(offset_momentum3_headers))    
        combinations3 = combinations3_headers[combinations3_index]  + combinations3_body
        advance3 = advance3_headers[advance3_index]  + advance3_body
        report_energy3 = report_energy3_headers[report_energy3_index]  + report_energy3_body
        offset_momentum3 = offset_momentum3_headers[offset_momentum3_index]  + offset_momentum3_body

        combinations4_index = random.randrange(0, len(combinations4_headers))
        advance4_index = random.randrange(0, len(advance4_headers))
        report_energy4_index = random.randrange(0, len(report_energy4_headers))
        offset_momentum4_index = random.randrange(0,len(offset_momentum4_headers))    
        combinations4 = combinations4_headers[combinations4_index]  + combinations4_body
        advance4 = advance4_headers[advance4_index]  + advance4_body
        report_energy4 = report_energy4_headers[report_energy4_index]  + report_energy4_body
        offset_momentum4 = offset_momentum4_headers[offset_momentum4_index]  + offset_momentum4_body

        combinations5_index = random.randrange(0, len(combinations5_headers))
        advance5_index = random.randrange(0, len(advance5_headers))
        report_energy5_index = random.randrange(0, len(report_energy5_headers))
        offset_momentum5_index = random.randrange(0,len(offset_momentum5_headers))    
        combinations5 = combinations5_headers[combinations5_index]  + combinations5_body
        advance5 = advance5_headers[advance5_index]  + advance5_body
        report_energy5 = report_energy5_headers[report_energy5_index]  + report_energy5_body
        offset_momentum5 = offset_momentum5_headers[offset_momentum5_index]  + offset_momentum5_body        
    
        program1 =  combinations1 + advance1 + report_energy1 + offset_momentum1 + rest1
        program2 =  combinations2 + advance2 + report_energy2 + offset_momentum2 + rest2
        program3 =  combinations3 + advance3 + report_energy3 + offset_momentum3 + rest3
        program4 =  combinations4 + advance4 + report_energy4 + offset_momentum4 + rest4
        program5 =  combinations5 + advance5 + report_energy5 + offset_momentum5 + rest5

        int_log_2_index = random.randrange(0, len(int_log_2_headers))
        FFT_num_flops_index = random.randrange(0, len(FFT_num_flops_headers))
        FFT_transform_internal_index = random.randrange(0,len(FFT_transform_internal_headers))
        FFT_bitreverse_index = random.randrange(0, len(FFT_bitreverse_headers))
        FFT_transform_index = random.randrange(0, len(FFT_transform_headers))
        #FFT_inverse_index = random.randrange(0, len(FFT_inverse_headers))
        
        int_log_2 = int_log_2_headers[int_log_2_index]  + int_log_2_body
        FFT_num_flops = FFT_num_flops_headers[FFT_num_flops_index]  + FFT_num_flops_body
        FFT_transform_internal = FFT_transform_internal_headers[FFT_transform_internal_index]  + FFT_transform_internal_body
        FFT_bitreverse = FFT_bitreverse_headers[FFT_bitreverse_index]  + FFT_bitreverse_body
        FFT_transform = FFT_transform_headers[FFT_transform_index]  + FFT_transform_body
        #FFT_inverse = FFT_inverse_headers[FFT_inverse_index]  + FFT_inverse_body

        programFFT =  int_log_2 + FFT_num_flops + FFT_transform_internal + FFT_bitreverse + FFT_transform  + restFFT
        
        prog +=   program1 + program2 + program3 + program4 + program5 + programFFT
    prog += final
    programName = "large" + str(i) + ".py"
    with open(programName, 'w+') as f:
        f.write(prog)
#print(prog)



                    
