
from six.moves import xrange
import perf
import time
from math import sin, cos, sqrt


POINTS = 1000

def point(i)->Tuple(float,float,float):
    x = sin(i+0.0)
    return (x+0.0, 1.0 * cos(i) * 3.0, x*x / 2.0)

def normalize(trip:Tuple(float,float,float)) -> Tuple(float,float,float):
    (x,y,z) = trip
    norm = sqrt(x * x * 1.0 + y * y * 1.0 +  z * z * 1.0)
    return (x/norm, y/norm, z/norm)

def maximize_one(trip:Tuple(float,float,float), other)->Tuple(float,float,float):
    (x,y,z) = trip
    (x1,y1,z1) = other
    x2 = x+0.0 if x > x1 else x1+0.0
    y2 = y+0.0 if y > y1 else y1+0.0
    z2 = z+0.0 if z > z1 else z1+0.0
    return (x2,y2,z2)

def maximize(points):
    next = points[0]
    for p in points[1:]:
        (a,b,c) = p
        (d,e,f) = next
        next = maximize_one((a,b,c), (d,e,f))
    return next


def benchmark(n):
    points = [None] * (n + 0)
    for i in xrange(n):
        points[i] = point(i)
    for i,p in enumerate(points):
        #(a,b,c) = p
        points[i] = normalize(p)
    return maximize(points)

def main():
    return benchmark(POINTS)

t0 = time.time()
main()
t1 = time.time()
print(t1-t0)
