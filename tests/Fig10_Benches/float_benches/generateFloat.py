import random

header = """
from six.moves import xrange
import perf
import time
from math import sin, cos, sqrt


POINTS = 1000

"""
point_headers = ["def point(i:float)->Tuple(float,float,float):",
                 "def point(i)->Tuple(float,float,float):"]
point_body = """
    x = sin(i+0.0)
    return (x+0.0, 1.0 * cos(i) * 3.0, x*x / 2.0)

"""

normalize_headers = ["def normalize(trip:Tuple(float,float,float)) -> Tuple(float,float,float):",
                     "def normalize(trip) -> Tuple(float,float,float):"]
normalize_body = """
    (x,y,z) = trip
    norm = sqrt(x * x * 1.0 + y * y * 1.0 +  z * z * 1.0)
    return (x/norm, y/norm, z/norm)

"""

maximize_one_headers = ["def maximize_one(trip:Tuple(float,float,float), other:Tuple(float,float,float))->Tuple(float,float,float):",
                        "def maximize_one(trip:Tuple(float,float,float), other)->Tuple(float,float,float):",
                        "def maximize_one(trip, other:Tuple(float,float,float))->Tuple(float,float,float):",
                        "def maximize_one(trip, other)->Tuple(float,float,float):"]
maximize_one_body = """
    (x,y,z) = trip
    (x1,y1,z1) = other
    x2 = x+0.0 if x > x1 else x1+0.0
    y2 = y+0.0 if y > y1 else y1+0.0
    z2 = z+0.0 if z > z1 else z1+0.0
    return (x2,y2,z2)

"""


maximize_headers = ["def maximize(points):",
                    "def maximize(points:List(Dyn)):"]
maximize_body = """
    next = points[0]
    for p in points[1:]:
        (a,b,c) = p
        (d,e,f) = next
        next = maximize_one((a,b,c), (d,e,f))
    return next

"""

rest = """
def benchmark(n):
    points = [None] * (n + 0)
    for i in xrange(n):
        points[i] = point(i)
    for i,p in enumerate(points):
        #(a,b,c) = p
        points[i] = normalize(p)
    return maximize(points)

def main():
    return benchmark(POINTS)

t0 = time.time()
main()
t1 = time.time()
print(t1-t0)
"""
i = 0
indexList = []
while i < 16:
    
    point_index = random.randrange(0,len(point_headers))
    normalize_index = random.randrange(0,len(normalize_headers))
    maximize_one_index = random.randrange(0,len(maximize_one_headers))
    maximize_index = 0
    if (point_index, normalize_index, maximize_one_index) not in indexList:
        indexList.append((point_index, normalize_index, maximize_one_index))
        i += 1
        point = point_headers[point_index] + point_body
        normalize = normalize_headers[normalize_index] + normalize_body
        maximize_one = maximize_one_headers[maximize_one_index] + maximize_one_body
        maximize = maximize_headers[maximize_index] + maximize_body
        with open("float" + str(i) + ".py", "w+") as f:
            out = header + point + normalize + maximize_one + maximize + rest
            f.write(out)
