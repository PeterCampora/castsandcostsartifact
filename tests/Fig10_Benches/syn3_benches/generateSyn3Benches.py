import random

programHead = """
from __future__ import division, print_function, absolute_import
from six.moves import xrange
from itertools import islice
import time
import random
from array import array
import math


from bisect import bisect


__contact__ = "collinwinter@google.com (Collin Winter)"
DEFAULT_ITERATIONS = 10000
DEFAULT_REFERENCE = 'sun'




SOLVE_ARG = 60

WIDTH, HEIGHT = 5, 10
DIR_NO = 6
S, E = WIDTH * HEIGHT, 2
SE = S + (E / 2)
SW = SE - E
W, NW, NE = -E, -SE, -SW

SOLUTIONS = [
    '00001222012661126155865558633348893448934747977799',
    '00001222012771127148774485464855968596835966399333',
    '00001222012771127148774485494855998596835966366333',
    '00001222012771127148774485994855948596835966366333',
    '00001222012771127166773863384653846538445584959999',
    '00001222012771127183778834833348555446554666969999',
    '00001222012771127183778834833348555446554966699996',
    '00001223012331123166423564455647456775887888979999',
    '00001555015541144177484726877268222683336689399993',
    '00001555015541144177484728677286222863338669399993',
    '00001599015591159148594482224827748276837766366333',
    '00001777017871184155845558449984669662932629322333',
    '00004222042664426774996879687759811598315583153331',
    '00004222042774427384773183331866118615586555969999',
    '00004223042334423784523785771855718566186611969999',
    '00004227042874428774528735833155831566316691169999',
    '00004333045534455384175781777812228116286662969999',
    '00004333045934459384559185991866118612286727267772',
    '00004333047734427384277182221866118615586555969999',
    '00004555045514411224172621768277368736683339899998',
    '00004555045514411224172721777299998966836688368333',
    '00004555045534413334132221177266172677886888969999',
    '00004555045534473334739967792617926192661882211888',
    '00004555045564466694699992288828811233317273177731',
    '00004555045564466694699997773172731233312881122888',
    '00004555045564496664999962288828811233317273177731',
    '00004555045564496664999967773172731233312881122888',
    '00004555045584411884171187722866792679236992369333',
    '00004555045584411884191189999832226333267372677766',
    '00004555045584411884191189999866222623336727367773',
    '13335138551389511895778697869947762446624022240000',
    '13777137271333211882888226999946669446554055540000',
    '13777137271333211882888229999649666446554055540000',
    '27776272768221681166819958195548395443954033340000',
    '33322392623926696648994485554855148117871077710000',
    '33366366773867284772842228449584195119551099510000',
    '33366366953869584955849958447784172117721022210000',
    '33366366953869589955849458447784172117721022210000',
    '33386388663866989999277712727142211441554055540000',
    '33396329963297629766822778117148811448554055540000',
    '33399366953869586955846458447784172117721022210000',
    '37776372763332622266899998119148811448554055540000',
    '39999396683336822268277682748477144114551055510000',
    '39999398663338622286277862748477144114551055510000',
    '66777627376233362223899998119148811448554055540000',
    '69999666945564455584333843887738172117721022210000',
    '88811228816629162971629776993743337443554055540000',
    '88822118821333213727137776999946669446554055540000',
    '88822118821333213727137779999649666446554055540000',
    '89999893338663786377286712627142211441554055540000',
    '99777974743984439884333685556855162116621022210000',
    '99995948554483564835648336837766172117721022210000',
    '99996119661366513855133853782547782447824072240000',
    '99996911668166581755817758732548732443324032240000',
    '99996926668261182221877718757148355443554033340000',
    '99996955568551681166812228177248372443774033340000',
    '99996955568551681166813338137748372447724022240000',
    '99996966645564455584333843887738172117721022210000',
    '99996988868877627166277112223143331443554055540000',
    '99997988878857765474655446532466132113321032210000']

"""

combinations_headers = ["def combinations(l):",
                        "def combinations(l)->List(Dyn):",
                        "def combinations(l:List(Dyn))->List(Dyn):"]

combinations_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


PI = 3.14159265358979323
SOLAR_MASS = 4 * PI * PI
DAYS_PER_YEAR = 365.24
#pdb.set_trace()
BODIES = {
    'sun': ([0.0, 0.0, 0.0], [0.0, 0.0, 0.0], SOLAR_MASS),

    'jupiter': ([4.84143144246472090e+00,
                 -1.16032004402742839e+00,
                 -1.03622044471123109e-01],
                [1.66007664274403694e-03 * DAYS_PER_YEAR,
                 7.69901118419740425e-03 * DAYS_PER_YEAR,
                 -6.90460016972063023e-05 * DAYS_PER_YEAR],
                9.54791938424326609e-04 * SOLAR_MASS),

    'saturn': ([8.34336671824457987e+00,
                4.12479856412430479e+00,
                -4.03523417114321381e-01],
               [(-2.76742510726862411e-03 * DAYS_PER_YEAR),
                (4.99852801234917238e-03 * DAYS_PER_YEAR),
                (2.30417297573763929e-05 * DAYS_PER_YEAR)],
               (2.85885980666130812e-04 * SOLAR_MASS)),

    'uranus': ([1.28943695621391310e+01,
                -1.51111514016986312e+01,
                -2.23307578892655734e-01],
               [2.96460137564761618e-03 * DAYS_PER_YEAR,
                2.37847173959480950e-03 * DAYS_PER_YEAR,
                -2.96589568540237556e-05 * DAYS_PER_YEAR],
               4.36624404335156298e-05 * SOLAR_MASS),

    'neptune': ([1.53796971148509165e+01,
                 -2.59193146099879641e+01,
                 1.79258772950371181e-01],
                [2.68067772490389322e-03 * DAYS_PER_YEAR,
                 1.62824170038242295e-03 * DAYS_PER_YEAR,
                 -9.51592254519715870e-05 * DAYS_PER_YEAR],
                5.15138902046611451e-05 * SOLAR_MASS)}


SYSTEM = list(BODIES.values())
PAIRS = combinations(SYSTEM)

"""

advance_headers = ["def advance(dt, n, bodies, pairs):",
                   "def advance(dt:float, n, bodies, pairs):",
                   "def advance(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy_headers = ["def report_energy(bodies, pairs, e):",
                         "def report_energy(bodies, pairs, e:float)->float:",
                         "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum_headers = ["def offset_momentum(ref, bodies, px, py, pz):",
                          "def offset_momentum(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest = """
def bench_nbody(loops, reference, iterations):
    # Set up global state
    offset_momentum(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy(SYSTEM, PAIRS, 0.0)
    return None

def main():
    bench_nbody(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

#bench_nbody(1,'sun',20000)
#t0 = time.time()
#main()
#t1 = time.time()
#print(t1-t0)
"""
combinations1_headers = ["def combinations1(l):",
                        "def combinations1(l)->List(Dyn):",
                        "def combinations1(l:List(Dyn))->List(Dyn):"]

combinations1_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


"""

advance1_headers = ["def advance1(dt, n, bodies, pairs):",
                   "def advance1(dt:float, n, bodies, pairs):",
                   "def advance1(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance1(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance1(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance1(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance1(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance1(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance1_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy1_headers = ["def report_energy1(bodies, pairs, e):",
                         "def report_energy1(bodies, pairs, e:float)->float:",
                         "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy1(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy1(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy1_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum1_headers = ["def offset_momentum1(ref, bodies, px, py, pz):",
                          "def offset_momentum1(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum1(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum1_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest1 = """
def bench_nbody1(loops, reference, iterations):
    # Set up global state
    offset_momentum1(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy1(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy1(SYSTEM, PAIRS, 0.0)
    return None

def main1():
    bench_nbody1(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

"""
combinations2_headers = ["def combinations2(l):",
                        "def combinations2(l)->List(Dyn):",
                        "def combinations2(l:List(Dyn))->List(Dyn):"]

combinations2_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


"""

advance2_headers = ["def advance2(dt, n, bodies, pairs):",
                   "def advance2(dt:float, n, bodies, pairs):",
                   "def advance2(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance2(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance2(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance2(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance2(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance2(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance2_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy2_headers = ["def report_energy2(bodies, pairs, e):",
                         "def report_energy2(bodies, pairs, e:float)->float:",
                         "def report_energy2(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy2(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy2(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy2(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy2(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy2(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy2(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy2_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum2_headers = ["def offset_momentum2(ref, bodies, px, py, pz):",
                          "def offset_momentum2(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum2(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum2(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum2(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum2(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum2(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum2_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest2 = """
def bench_nbody2(loops, reference, iterations):
    # Set up global state
    offset_momentum2(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy2(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy2(SYSTEM, PAIRS, 0.0)
    return None

def main2():
    bench_nbody2(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

"""

combinations3_headers = ["def combinations3(l):",
                        "def combinations3(l)->List(Dyn):",
                        "def combinations3(l:List(Dyn))->List(Dyn):"]

combinations3_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


"""

advance3_headers = ["def advance3(dt, n, bodies, pairs):",
                   "def advance3(dt:float, n, bodies, pairs):",
                   "def advance3(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance3(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance3(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance3(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance3(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance3(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance3_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy3_headers = ["def report_energy3(bodies, pairs, e):",
                         "def report_energy3(bodies, pairs, e:float)->float:",
                         "def report_energy3(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy3(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy3(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy3(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy3(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy3(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy3(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy3_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum3_headers = ["def offset_momentum3(ref, bodies, px, py, pz):",
                          "def offset_momentum3(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum3(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum3(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum3(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum3(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum3(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum3_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest3 = """
def bench_nbody3(loops, reference, iterations):
    # Set up global state
    offset_momentum3(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy3(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy3(SYSTEM, PAIRS, 0.0)
    return None

def main3():
    bench_nbody3(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

"""

combinations4_headers = ["def combinations4(l):",
                        "def combinations4(l)->List(Dyn):",
                        "def combinations4(l:List(Dyn))->List(Dyn):"]

combinations4_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


"""

advance4_headers = ["def advance4(dt, n, bodies, pairs):",
                   "def advance4(dt:float, n, bodies, pairs):",
                   "def advance4(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance4(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance4(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance4(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance4(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance4(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance4_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy4_headers = ["def report_energy4(bodies, pairs, e):",
                         "def report_energy4(bodies, pairs, e:float)->float:",
                         "def report_energy4(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy4(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy4(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy4(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy4(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy4(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy4(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy4_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum4_headers = ["def offset_momentum4(ref, bodies, px, py, pz):",
                          "def offset_momentum4(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum4(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum4(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum4(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum4(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum4(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum4_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest4 = """
def bench_nbody4(loops, reference, iterations):
    # Set up global state
    offset_momentum4(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy4(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy4(SYSTEM, PAIRS, 0.0)
    return None

def main4():
    bench_nbody4(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

"""

combinations5_headers = ["def combinations5(l):",
                        "def combinations5(l)->List(Dyn):",
                        "def combinations5(l:List(Dyn))->List(Dyn):"]

combinations5_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result


"""

advance5_headers = ["def advance5(dt, n, bodies, pairs):",
                   "def advance5(dt:float, n, bodies, pairs):",
                   "def advance5(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance5(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                   "def advance5(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance5(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance5(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                   "def advance5(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance5_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy5_headers = ["def report_energy5(bodies, pairs, e):",
                         "def report_energy5(bodies, pairs, e:float)->float:",
                         "def report_energy5(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy5(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                         "def report_energy5(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                         "def report_energy5(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy5(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                         "def report_energy5(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                         "def report_energy5(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]

report_energy5_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum5_headers = ["def offset_momentum5(ref, bodies, px, py, pz):",
                          "def offset_momentum5(ref, bodies, px:float, py:float, pz:float):",
                          "def offset_momentum5(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                          "def offset_momentum5(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                          "def offset_momentum5(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum5(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                          "def offset_momentum5(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum5_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None

"""

rest5 = """
def bench_nbody5(loops, reference, iterations):
    # Set up global state
    offset_momentum5(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy5(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy5(SYSTEM, PAIRS, 0.0)
    return None

def main5():
    bench_nbody5(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)

"""

int_log_2_headers = ["def int_log2(n):", "def int_log2(n:float):"]
int_log_2_body = """
    k = 1
    log = 0
    while k < (n+0.0):
        k *= 2
        log += 1
    if n != 1 << log:
        raise Exception("FFT: Data length is not a power of 2: %s" % n)
    return (log)

"""

FFT_num_flops_headers = ["def FFT_num_flops(N):"
                         ,"def FFT_num_flops(N:float)->float:"]
FFT_num_flops_body = """
    return (5.0 * N - 2) * int_log2(N) + 2 * (N + 1)

"""
FFT_transform_internal_headers = ["def FFT_transform_internal(N, data, direction):",
                                  "def FFT_transform_internal(N:float, data, direction):",
                                  "def FFT_transform_internal(N:float, data, direction:float):"]

FFT_transform_internal_body = """
    n = int(N // 2)
    bit = 0
    dual = 1
    if n == 1:
        return None

    logn = int_log2(n)
    if N == 0:
        return None
    FFT_bitreverse(N, data)

    # apply fft recursion
    # this loop executed int_log2(N) times
    bit = 0
    while bit < logn:
        w_real = 1.0
        w_imag = 0.0
        theta = 2.0 * direction * math.pi / (2.0 * float(dual))
        s = math.sin(theta)
        t = math.sin(theta / 2.0)
        s2 = 2.0 * t * t
        for b in range(0, n, 2 * dual):
            i = 2 * b
            j = 2 * (b + dual)
            wd_real = data[j]
            wd_imag = data[j + 1]
            data[j] = data[i] - wd_real
            data[j + 1] = data[i + 1] - wd_imag
            data[i] += wd_real
            data[i + 1] += wd_imag
        for a in xrange(1, dual):
            tmp_real = w_real - s * w_imag - s2 * w_real
            tmp_imag = w_imag + s * w_real - s2 * w_imag
            w_real = tmp_real
            w_imag = tmp_imag
            for b in range(0, n, 2 * dual):
                i = 2 * (b + a)
                j = 2 * (b + a + dual)
                z1_real = data[j]
                z1_imag = data[j + 1]
                wd_real = w_real * z1_real - w_imag * z1_imag
                wd_imag = w_real * z1_imag + w_imag * z1_real
                data[j] = data[i] - wd_real
                data[j + 1] = data[i + 1] - wd_imag
                data[i] += wd_real
                data[i + 1] += wd_imag
        bit += 1
        dual *= 2
        return None

"""
FFT_bitreverse_headers = ["def FFT_bitreverse(N, data):"
                          ,"def FFT_bitreverse(N:float, data):"]

FFT_bitreverse_body = """
    n = int(N // 2)
    nm1 = n - 1
    j = 0
    for i in range(nm1):
        ii = i << 1
        jj = j << 1
        k = n >> 1
        if i < j:
            tmp_real = data[ii]
            tmp_imag = data[ii + 1]
            data[ii] = data[jj]
            data[ii + 1] = data[jj + 1]
            data[jj] = tmp_real
            data[jj + 1] = tmp_imag
        while k <= j:
            j -= k
            k >>= 1
        j += k
        return None

"""

FFT_transform_headers = ["def FFT_transform(N, data):"
, "def FFT_transform(N:float, data):"]

FFT_transform_body = """
    FFT_transform_internal(N, data, -1)
    return None

"""

FFT_inverse_headers = ["def FFT_inverse(N, data):"
                       ,"def FFT_inverse(N:float, data):"]
FFT_inverse_body = """
    n = N / 2
    norm = 0.0
    FFT_transform_internal(N, data, +1)
    norm = 1 / float(n)
    for i in xrange(N):
        data[i] *= norm
    return None

"""

restFFT = """
def bench_FFT(loops, N, cycles):
    twoN = 2 * N
    init_vec = array('d', [random.random() for i in range(twoN)]) #Random(7).RandomVector(twoN)
    range_it = xrange(loops)
    #t0 = perf.perf_counter()

    for _ in range_it:
        x = array('d')
        x[:] = init_vec[:]
        #x = copy_vector(init_vec)
        for i in xrange(cycles):
            FFT_transform(twoN, x)
            FFT_inverse(twoN, x)
            
    return None
    #return perf.perf_counter() - t0

"""




rotate_headers = ["def rotate(ido)->List(Dyn):", "def rotate(ido:List(Dyn))->List(Dyn):"]
rotate_body = """
    for _ in ido:
        break
    rd={E: NE, NE: NW, NW: W, W: SW, SW: SE, SE: E}
    return [rd[o] for o in ido]

"""


flip_headers = ["def flip(ido)->List(Dyn):", "def flip(ido:List(Dyn))->List(Dyn):"]
flip_body =    """
    for _ in ido:
        break
    fd={E: E, NE: SE, NW: SW, W: W, SW: NW, SE: NE}
    return [fd[o] for o in ido]

"""


permute_headers = ["def permute(ido, r_ido)->List(Dyn):",
                   "def permute(ido:List(Dyn), r_ido)->List(Dyn):",
                   "def permute(ido, r_ido:List(Dyn))->List(Dyn):",
                   "def permute(ido:List(Dyn), r_ido:List(Dyn))->List(Dyn):"]
permute_body = """
    #for _ in ido:
    #    break
    #for _ in r_ido:
    #    break
    #ps = list(ido)
    ps = []
    ps.append(ido)
    for r in xrange(DIR_NO - 1):
        #rotate(list(ido))
    #    for pp in list(ido):
    #        flip(list(ido))
            
        ps.append(rotate(ps[-1]))
        #if ido == r_ido:                 # C2-symmetry
            #ps = ps[0:DIR_NO // 2]
        for i in range(len(ps)):
            pp = ps[i]
            ps.append(flip(pp))
    return ps
    #return []

"""

convert_headers = ["def convert(ido)->List(Dyn):", "def convert(ido:List(Dyn))->List(Dyn):"]
convert_body = """
    '''incremental direction offsets -> "coordinate offsets" '''
    out = [0.0]
    for o in ido:
        out.append(out[-1] + o)
    return list(set(out))

"""


get_footprints_headers = ["def get_footprints(board, cti, pieces)->List(List(List(Dyn))):",
                          "def get_footprints(board:List(Dyn), cti, pieces)->List(List(List(Dyn))):",                          
                          "def get_footprints(board, cti, pieces:List(Dyn))->List(List(List(Dyn))):",                          
                          "def get_footprints(board:List(Dyn), cti, pieces:List(Dyn))->List(List(List(Dyn))):"]
get_footprints_body = """        
    for _ in pieces:
        break
    fps = [[[] for p in xrange(len(pieces))] for ci in xrange(len(board))]
    for c in board:
        for pi, p in enumerate(pieces):
            for pp in p:
                fp = frozenset([cti[c + o] for o in pp if (c + o) in cti])
                if len(fp) == 5:
                    fps[min(fp)][pi].append(fp)
    return fps

"""

get_senh_headers = ["def get_senh(board, cti)->List(Dyn):",
                    "def get_senh(board:List(Dyn), cti)->List(Dyn):"]
get_senh_body = """
    '''-> south-east neighborhood'''
    se_nh = []
    nh = [E, SW, SE]
    for c in board:
        se_nh.append(frozenset([cti[c + o] for o in nh if (c + o) in cti]))
    return se_nh

"""

get_puzzle_headers = ["def get_puzzle(width, height)->Tuple(Dyn, Dyn, List(Dyn)):",
                      "def get_puzzle(width:int, height)->Tuple(Dyn, Dyn, List(Dyn)):",
                      "def get_puzzle(width, height:int)->Tuple(Dyn, Dyn, List(Dyn)):",
                      "def get_puzzle(width:int, height:int)->Tuple(Dyn, Dyn, List(Dyn)):"]
get_puzzle_body = """
    board = [E * x + S * y + (y % 2)
             for y in xrange(height+0)
             for x in xrange(width+0)]
    cti = dict((board[i], i) for i in xrange(len(board)))

    # Incremental direction offsets
    idos = [[E, E, E, SE],
            [SE, SW, W, SW],
            [W, W, SW, SE],
            [E, E, SW, SE],
            [NW, W, NW, SE, SW],
            [E, E, NE, W],
            [NW, NE, NE, W],
            [NE, SE, E, NE],
            [SE, SE, E, SE],
            [E, NW, NW, NW]]

    # Restrict piece 4
    perms = (permute(p, idos[3]) for p in idos)    
    #pieces = [[convert(pp) for pp in p] for p in perms] #this is original code
    p = list(perms) #new code Only due one iteration of previous nested loop from above
    pieces = [[convert(pp) for pp in p[0]]]
    return (board, cti, pieces)
    #return (board, cti, [])

"""

solve_headers = ["def solve(n, i_min, free, curr_board, pieces_left, solutions, fps, se_nh):"]
solve_body = """
          # Hack to use a fast local variable to avoid a global lookup

    fp_i_cands = fps[i_min]
    for p in pieces_left:
        fp_cands = fp_i_cands[p]
        for fp in fp_cands:
            if fp <= free:
                n_curr_board = curr_board[:]
                for ci in fp:
                    n_curr_board[ci] = p

                if len(pieces_left) > 1:
                    n_free = free - fp
                    n_i_min = min(n_free)
                    if len(n_free & se_nh[n_i_min]) > 0:
                        n_pieces_left = pieces_left[:]
                        n_pieces_left.remove(p)
                        solve(n, n_i_min, n_free, n_curr_board,
                              n_pieces_left, solutions, fps, se_nh)
                else:
                    s = ''.join(map(str, n_curr_board))
                    solutions.insert(bisect(solutions, s), s)
                    rs = s[::-1]
                    solutions.insert(bisect(solutions, rs), rs)
                    if len(solutions) >= n:
                        return None

        if len(solutions) >= n:
            return None

"""

final = """
def main():
    t0 = time.time()
    twoN = 2 * 1024
    init_vec = array('d', [random.random() for i in range(twoN)]) #Random(7).RandomVector(twoN)
    FFT_bitreverse(twoN, init_vec)
    offset_momentum(BODIES[DEFAULT_REFERENCE], SYSTEM, 0.0, 0.0, 0.0)
    for i in range(5):        
        board, cti, pieces = get_puzzle(WIDTH, HEIGHT)    
        fps = get_footprints(board, cti, pieces)
        se_nh = get_senh(board, cti)
    
    #bench_nbody5(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)
    #bench_FFT(10, 1024, 50)
    t1 = time.time()
    print(t1-t0)
    return None

main()
"""

indexList = []
i = 1
while i <= 100:
#for i in range(100):
    prog = programHead
    combinations_index = random.randrange(0, len(combinations_headers))
    advance_index = random.randrange(0, len(advance_headers))
    report_energy_index = random.randrange(0, len(report_energy_headers))
    offset_momentum_index = random.randrange(0,len(offset_momentum_headers))    
    combinations = combinations_headers[combinations_index]  + combinations_body
    advance = advance_headers[advance_index]  + advance_body
    report_energy = report_energy_headers[report_energy_index]  + report_energy_body
    offset_momentum = offset_momentum_headers[offset_momentum_index]  + offset_momentum_body
    program =  combinations + advance + report_energy + offset_momentum

    int_log_2_index = random.randrange(0, len(int_log_2_headers))
    FFT_num_flops_index = random.randrange(0, len(FFT_num_flops_headers))
    FFT_transform_internal_index = random.randrange(0,len(FFT_transform_internal_headers))
    FFT_bitreverse_index = random.randrange(0, len(FFT_bitreverse_headers))
    FFT_transform_index = random.randrange(0, len(FFT_transform_headers))
    FFT_inverse_index = random.randrange(0, len(FFT_inverse_headers))


    int_log_2 = int_log_2_headers[int_log_2_index]  + int_log_2_body
    FFT_num_flops = FFT_num_flops_headers[FFT_num_flops_index]  + FFT_num_flops_body
    FFT_transform_internal = FFT_transform_internal_headers[FFT_transform_internal_index]  + FFT_transform_internal_body
    FFT_bitreverse = FFT_bitreverse_headers[FFT_bitreverse_index]  + FFT_bitreverse_body
    FFT_transform = FFT_transform_headers[FFT_transform_index]  + FFT_transform_body
    FFT_inverse = FFT_inverse_headers[FFT_inverse_index]  + FFT_inverse_body
    programFFT =  int_log_2 + FFT_num_flops + FFT_transform_internal + FFT_bitreverse + FFT_transform + FFT_inverse + restFFT


    rotate_index = random.randrange(len(rotate_headers))
    flip_index = random.randrange(len(flip_headers))
    permute_index = random.randrange(len(permute_headers))
    convert_index = random.randrange(len(convert_headers))
    get_footprints_index = random.randrange(len(get_footprints_headers))
    get_senh_index = random.randrange(len(get_senh_headers))
    get_puzzle_index = random.randrange(len(get_puzzle_headers))
    solve_index = random.randrange(len(solve_headers))

    rotate = rotate_headers[rotate_index] + rotate_body
    flip = flip_headers[flip_index] + flip_body
    permute = permute_headers[permute_index] + permute_body
    convert = convert_headers[convert_index] + convert_body
    get_footprints = get_footprints_headers[get_footprints_index] + get_footprints_body
    get_senh = get_senh_headers[get_senh_index] + get_senh_body
    get_puzzle = get_puzzle_headers[get_puzzle_index] + get_puzzle_body
    solve = solve_headers[solve_index] + solve_body
    programMeteor = rotate + flip + permute + convert + get_footprints + get_senh + get_puzzle + solve

    index_tup = (FFT_bitreverse_index, offset_momentum_index, get_puzzle_index, get_footprints_index, get_senh_index)
    #if(index_tup not in indexList):
    if(True): #with the variability in configurations, duplicate types for the functions that matter are likely
        indexList.append(index_tup)
        prog += program
        prog += programFFT
        prog+= programMeteor
        b = 0
        for a in range(100):

            program1 = ""
            #for b in range(1,6):
            combinations1_base = "def combinations" + str(b) + str(a) + "(l):"
            combinations1 = combinations1_base + combinations1_body
            advance1_base = "def advance" + str(b) + str(a) + "(dt, n, bodies, pairs):"
            advance1 = advance1_base  + advance1_body
            report_energy1_base = "def report_energy" + str(b) + str(a) + "(bodies, pairs, e):"
            report_energy1 = report_energy1_base  + report_energy1_body
            offset_momentum1_base = "def offset_momentum" + str(b) + str(a) + "(ref, bodies, px, py, pz):"
            offset_momentum1 = offset_momentum1_base + offset_momentum1_body



            program1 +=  combinations1 + advance1 + report_energy1 + offset_momentum1 
            prog += program1

        
            int_log_2_base = "def int_log2" + str(a) + "(n):"
            int_log_2 = int_log_2_base  + int_log_2_body
            FFT_num_flops_base = "def FFT_num_flops" + str(a) + "(N):"
            FFT_num_flops = FFT_num_flops_base  + FFT_num_flops_body
            FFT_transform_internal_base = "def FFT_transform_internal" + str(a) + "(N, data, direction):"
            FFT_transform_internal = FFT_transform_internal_base  + FFT_transform_internal_body
            FFT_bitreverse_base = "def FFT_bitreverse" + str(a) + "(N, data):"
            FFT_bitreverse = FFT_bitreverse_base  + FFT_bitreverse_body
            FFT_transform_base = "def FFT_transform" + str(a) + "(N, data):"
            FFT_transform = FFT_transform_base  + FFT_transform_body
            FFT_inverse_base = "def FFT_inverse" + str(a) + "(N, data):"
            FFT_inverse = FFT_inverse_base  + FFT_inverse_body

            programFFT =  int_log_2 + FFT_num_flops + FFT_transform_internal + FFT_bitreverse + FFT_transform + FFT_inverse 
        
            prog += programFFT
        prog += final
        i += 1
        programName = "synthetic_3_" + str(i) + ".py"
        with open(programName, 'w+') as f:
            f.write(prog)
