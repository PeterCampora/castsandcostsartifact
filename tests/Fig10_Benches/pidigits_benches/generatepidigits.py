import random
header = """
import itertools
import time
from six.moves import map as imap


DEFAULT_DIGITS = 2000
icount = itertools.count
islice = itertools.islice


def gen_x():
    #return imap(lambda k: (k, iadd(imult(4, k), 2), 0, iadd(imult(2, k), 1)), icount(1))
    return imap(lambda k: (k, 4 * k + 2, 0, 2 * k + 1), icount(1))

"""

compose_headers = ["def compose(a, b)->Tuple(int,int,int,int):",
                   "def compose(a:Tuple(int,int,int,int), b)->Tuple(int,int,int,int):",
                   "def compose(a, b:Tuple(int,int,int,int))->Tuple(int,int,int,int):",
                   "def compose(a:Tuple(int,int,int,int), b:Tuple(int,int,int,int))->Tuple(int,int,int,int):"]

compose_body = """    
    aq, ar, as_, at = a
    bq, br, bs, bt = b
    return (aq * bq + 0,
            aq * br + ar * bt + 0,
            as_ * bq + at * bs + 0,
            as_ * br + at * bt + 0)

"""

extract_headers = ["def extract(z, j)->int:",
                   "def extract(z:Tuple(int,int,int,int), j) -> int:",
                   "def extract(z, j:int) -> int:",
                   "def extract(z:Tuple(int,int,int,int), j:int) -> int:"]
extract_body = """
    q, r, s, t = z
    #return idiv((iadd(imult(q, j), r)), (iadd(imult(s, j),  t)))
    return (q * j + r + 0) // (s * j + t + 0)
"""

rest = """
def gen_pi_digits():
    z = (1, 0, 0, 1)
    x = gen_x()
    while True:
        y = extract(z, 3)
        while y != extract(z, 4):
            z = compose(z, next(x))
            y = extract(z, 3)
        #z = compose((10, imult(-10, y), 0, 1), z)
        z = compose((10, (-10 * y), 0, 1), z)
        yield y

def calc_ndigits(n):
#def calc_ndigits(n) -> List(Dyn):
    return list(islice(gen_pi_digits(), n))

def main():
    calc_ndigits(100)

t0 = time.time()
main()
t1 = time.time()
print(t1-t0)
"""
indexList = []
i = 0
while i < 16:
    compose_index = random.randrange(0,len(compose_headers))
    extract_index = random.randrange(0,len(extract_headers))
    if (compose_index, extract_index) not in indexList:
        i +=1
        indexList.append((compose_index, extract_index))
        compose= compose_headers[compose_index] + compose_body
        extract = extract_headers[extract_index] + extract_body
    
        out = header + compose + extract + rest
        with open("pi" + str(i) + ".py", "w+") as f:
            f.write(out)
