echo "float"
cd float_benches
python3 sortTimes.py
cd ..
echo ""

echo "meteor_contest"
cd meteor_contest_benches
python3 sortTimes.py
cd ..
echo ""

echo "nbody"
cd nbody_benches
python3 sortTimes.py
cd ..
echo ""

echo "pidigits"
cd pidigits_benches
python3 sortTimes.py
cd ..
echo ""

echo "raytrace"
cd raytrace_benches/
python3 sortTimes.py
cd ..
echo ""

echo "sm(fft)"
cd fft_benches
python3 sortTimes.py
cd ..
echo ""

echo "sm(MC)"
cd monte_carlo_benches
python3 sortTimes.py
cd ..
echo ""

echo "sm(SOR)"
cd sci_mark_benches
python3 sortTimes.py
cd ..
echo ""

echo "spectral_norm"
cd spectral_norm_benches
python3 sortTimes.py
cd ..
echo ""

echo "syn_1"
cd syn1_benches
python3 sortTimes.py
cd ..
echo ""

echo "syn_2"
cd syn2_benches
python3 sortTimes.py
cd ..
echo ""

echo "syn_3"
cd syn3_benches
python3 sortTimes.py
cd ..
echo ""
