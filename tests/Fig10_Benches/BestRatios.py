def singleBench(name):
    timeList = []
    with open(name + "_benches/log.txt", 'r') as f:
        for i,l in enumerate(f.readlines()):
            if (i % 2 == 0):
                continue
            else:
                timeList.append(eval(l))    
        HerderTime = timeList[0]
        sTimes = sorted(timeList)
        print(name)
        BestTime = sTimes[0]
        print(BestTime / HerderTime)

for n in ["fft", "float", "meteor_contest", "monte_carlo", "nbody", "pidigits", "raytrace", "sci_mark", "spectral_norm", "syn1", "syn2", "syn3"]:
    singleBench(n)
