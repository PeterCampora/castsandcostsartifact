import random
programHead = """import random
import time
from array import array
import math

import perf
from six.moves import xrange

"""


int_log_2_headers = ["def int_log2(n):", "def int_log2(n:float):"]
int_log_2_body = """
    k = 1
    log = 0
    while k < (n+0.0):
        k *= 2
        log += 1
    if n != 1 << log:
        raise Exception("FFT: Data length is not a power of 2: %s" % n)
    return (log)

"""

FFT_num_flops_headers = ["def FFT_num_flops(N)->float:"
                         ,"def FFT_num_flops(N:float)->float:"]
FFT_num_flops_body = """
    return (5.0 * N - 2) * int_log2(N) + 2 * (N + 1)

"""
FFT_transform_internal_headers = ["def FFT_transform_internal(N, data, direction):",
                                  "def FFT_transform_internal(N:float, data, direction):",
                                  "def FFT_transform_internal(N:float, data, direction:float):"]

FFT_transform_internal_body = """

    n = int(N // 2)
    bit = 0
    dual = 1
    if n == 1:
        return None

    logn = int_log2(n)
    if N == 0:
        return None
    FFT_bitreverse(N, data)

    # apply fft recursion
    # this loop executed int_log2(N) times
    bit = 0
    while bit < logn:
        w_real = 1.0
        w_imag = 0.0
        theta = 2.0 * direction * math.pi / (2.0 * float(dual))
        s = math.sin(theta)
        t = math.sin(theta / 2.0)
        s2 = 2.0 * t * t
        for b in range(0, n, 2 * dual):
            i = 2 * b
            j = 2 * (b + dual)
            wd_real = data[j]
            wd_imag = data[j + 1]
            data[j] = data[i] - wd_real
            data[j + 1] = data[i + 1] - wd_imag
            data[i] += wd_real
            data[i + 1] += wd_imag
        for a in xrange(1, dual):
            tmp_real = w_real - s * w_imag - s2 * w_real
            tmp_imag = w_imag + s * w_real - s2 * w_imag
            w_real = tmp_real
            w_imag = tmp_imag
            for b in range(0, n, 2 * dual):
                i = 2 * (b + a)
                j = 2 * (b + a + dual)
                z1_real = data[j]
                z1_imag = data[j + 1]
                wd_real = w_real * z1_real - w_imag * z1_imag
                wd_imag = w_real * z1_imag + w_imag * z1_real
                data[j] = data[i] - wd_real
                data[j + 1] = data[i + 1] - wd_imag
                data[i] += wd_real
                data[i + 1] += wd_imag
        bit += 1
        dual *= 2
        return None

"""
FFT_bitreverse_headers = ["def FFT_bitreverse(N, data):"
                          ,"def FFT_bitreverse(N:float, data):"]

FFT_bitreverse_body = """
    n = int(N // 2)
    nm1 = n - 1
    j = 0
    for i in range(nm1):
        ii = i << 1
        jj = j << 1
        k = n >> 1
        if i < j:
            tmp_real = data[ii]
            tmp_imag = data[ii + 1]
            data[ii] = data[jj]
            data[ii + 1] = data[jj + 1]
            data[jj] = tmp_real
            data[jj + 1] = tmp_imag
        while k <= j:
            j -= k
            k >>= 1
        j += k
        return None

"""

FFT_transform_headers = ["def FFT_transform(N, data):"
, "def FFT_transform(N:float, data):"]

FFT_transform_body = """
    FFT_transform_internal(N, data, -1)
    return None

"""

FFT_inverse_headers = ["def FFT_inverse(N, data):"
                       ,"def FFT_inverse(N:float, data):"]
FFT_inverse_body = """
    n = N / 2
    norm = 0.0
    FFT_transform_internal(N, data, +1)
    norm = 1 / float(n)
    for i in xrange(N):
        data[i] *= norm
    return None

"""

rest = """
def bench_FFT(loops, N, cycles):
    twoN = 2 * N
    init_vec = array('d', [random.random() for i in range(twoN)]) #Random(7).RandomVector(twoN)
    range_it = xrange(loops)
    #t0 = perf.perf_counter()

    for _ in range_it:
        x = array('d')
        x[:] = init_vec[:]
        #x = copy_vector(init_vec)
        for i in xrange(cycles):
            FFT_transform(twoN, x)
            FFT_inverse(twoN, x)
            
    return None
    #return perf.perf_counter() - t0

def main():
    t0 = time.time()
    bench_FFT(10, 1024, 50)
    t1 = time.time()
    print(t1-t0)
    return None

main()
"""

indexList = []
i = 1
while i <= 100:

    int_log_2_index = random.randrange(0, len(int_log_2_headers))
    FFT_num_flops_index = random.randrange(0, len(FFT_num_flops_headers))
    FFT_transform_internal_index = random.randrange(0,len(FFT_transform_internal_headers))
    FFT_bitreverse_index = random.randrange(0, len(FFT_bitreverse_headers))
    FFT_transform_index = random.randrange(0, len(FFT_transform_headers))
    FFT_inverse_index = random.randrange(0, len(FFT_inverse_headers))

    if not (int_log_2_index, FFT_num_flops_index, FFT_transform_internal_index, FFT_bitreverse_index, FFT_transform_index, FFT_inverse_index) in indexList:
        indexList.append((int_log_2_index, FFT_num_flops_index, FFT_transform_internal_index, FFT_bitreverse_index, FFT_transform_index, FFT_inverse_index))
        i += 1
        int_log_2 = int_log_2_headers[int_log_2_index]  + int_log_2_body
        FFT_num_flops = FFT_num_flops_headers[FFT_num_flops_index]  + FFT_num_flops_body
        FFT_transform_internal = FFT_transform_internal_headers[FFT_transform_internal_index]  + FFT_transform_internal_body
        FFT_bitreverse = FFT_bitreverse_headers[FFT_bitreverse_index]  + FFT_bitreverse_body
        FFT_transform = FFT_transform_headers[FFT_transform_index]  + FFT_transform_body
        FFT_inverse = FFT_inverse_headers[FFT_inverse_index]  + FFT_inverse_body

        program = programHead + int_log_2 + FFT_num_flops + FFT_transform_internal + FFT_bitreverse + FFT_transform + FFT_inverse + rest
        programName = "fft" + str(i) + ".py"
        with open(programName, 'w+') as f:
            f.write(program)
        #print(program)
