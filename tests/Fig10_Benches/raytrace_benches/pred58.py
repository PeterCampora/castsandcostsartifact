

import array
import math
import time
import perf
import pdb
from six.moves import xrange

"""
Assign the function vector the type: Function(['initx:Dyn', 'inity:Dyn', 'initz:Dyn'], Tuple(Dyn,Dyn,Dyn))
Assign the function dot the type: Function(['vec:Tuple(Dyn,Dyn,Dyn)', 'other:Tuple(Float,Float,Float)'], Float)
Assign the function magnitude the type: Function(['vec:Tuple(Dyn,Dyn,Float)'], Float)
Assign the function add the type: Function(['vec:Tuple(Dyn,Dyn,Dyn)', 'other:Choice 32<Dyn, Tuple(Dyn,Dyn,Dyn)>'], Tuple(Dyn,Dyn,Dyn))
Assign the function sub the type: Function(['vec:Choice 10<Dyn, Tuple(Dyn,Dyn,Dyn)>', 'other:Choice 11<Dyn, Tuple(Dyn,Dyn,Dyn)>'], Tuple(Dyn,Dyn,Dyn))
Assign the function scale the type: Function(['vec:Tuple(Dyn,Dyn,Dyn)', 'factor:Dyn'], Tuple(Dyn,Dyn,Dyn))
Assign the function cross the type: Function(['vec:Choice 1<Dyn, Tuple(Dyn,Dyn,Dyn)>', 'other:Tuple(Dyn,Dyn,Dyn)'], Tuple(Dyn,Dyn,Dyn))
Assign the function normalized the type: Function(['vec:Tuple(Dyn,Dyn,Dyn)'], Tuple(Dyn,Dyn,Dyn))
Assign the function negated the type: Function(['vec:Choice 38<Dyn, Tuple(Dyn,Dyn,Dyn)>'], Tuple(Dyn,Dyn,Dyn))
Assign the function eq the type: Function(['vec:Choice 18<Dyn, Tuple(Dyn,Dyn,Dyn)>', 'other:Choice 19<Dyn, Tuple(Dyn,Dyn,Dyn)>'], Bool)
Assign the function reflectThrough the type: Function(['vec:Choice 47<Dyn, Tuple(Dyn,Dyn,Dyn)>', 'normal:Choice 48<Dyn, Tuple(Float,Float,Float)>'], Tuple(Dyn,Dyn,Dyn))
Assign the function sphere the type: Function(['centre:Dyn', 'radius:Dyn'], Tuple(Dyn,Dyn))
Assign the function ray the type: Function(['point:Dyn', 'vect:Choice 17<Dyn, Tuple(Dyn,Dyn,Dyn)>'], Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))
Assign the function pointAtTime the type: Function(['ray:Choice 34<Dyn, Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn))>', 't:Dyn'], Tuple(Dyn,Dyn,Dyn))
Assign the function intersectionTime the type: Function(['s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn)', 'ray:Choice 13<Dyn, Tuple(Choice 11<Dyn, Tuple(Dyn,Dyn,Dyn)>,Tuple(Float,Float,Float))>'], Void)
Assign the function normalAt the type: Function(['s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn)', 'p:Tuple(Dyn,Dyn,Dyn)'], Tuple(Dyn,Dyn,Dyn))
Assign the function halfspace the type: Function(['point:Dyn', 'normal:Tuple(Dyn,Dyn,Dyn)'], Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))
Assign the function canvas the type: Function(['width:Choice 6<Dyn, Int>', 'height:Choice 7<Dyn, Int>'], Tuple(Dyn,Dyn,Int))
Assign the function plot the type: Function(['canv:Choice 42<Dyn, Tuple(Dyn,Int,Int)>', 'x:Int', 'y:Choice 43<Dyn, Int>', 'r:Choice 44<Dyn, Int>', 'g:Choice 45<Dyn, Int>', 'b:Choice 46<Dyn, Int>'], Void)
Assign the function firstIntersection the type: Function(['intersections:Choice 22<Dyn, List(Dyn)>'], Dyn)
Assign the function scene the type: Function([], Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Int))
Assign the function moveTo the type: Function(['sc:Choice 14<Dyn, Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)>', 'p:Dyn'], Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn))
Assign the function lookAt the type: Function(['sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)', 'p:Dyn'], Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn))
Assign the function addObject the type: Function(['sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)', 'object:Dyn', 'surface:Dyn'], Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn))
Assign the function addLight the type: Function(['sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)', 'p:Dyn'], Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn))
Assign the function addColours the type: Function(['a:Dyn', 'scale:Dyn', 'b:Dyn'], Tuple(Dyn,Dyn,Dyn))
Assign the function baseColourAt the type: Function(['ss:Choice 30<Dyn, Tuple(Dyn,Dyn,Dyn,Dyn)>', 'p:Dyn'], Dyn)
Assign the function render the type: Function(['sc:Choice 23<Dyn, Tuple(Dyn,Dyn,Choice 11<Dyn, Tuple(Dyn,Dyn,Dyn)>,Choice 10<Dyn, Tuple(Dyn,Dyn,Dyn)>,Float,Dyn)>', 'canvas1:Choice 24<Dyn, Tuple(Dyn,Int,Int)>'], Void)
Assign the function lightIsVisible the type: Function(['sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)', 'l:Choice 4<Dyn, Choice 10<Dyn, Tuple(Dyn,Dyn,Dyn)>>', 'p:Choice 5<Dyn, Choice 11<Dyn, Tuple(Dyn,Dyn,Dyn)>>'], Bool)
Assign the function visibleLights the type: Function(['sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)', 'p:Choice 8<Dyn, Choice 5<Dyn, Choice 11<Dyn, Tuple(Dyn,Dyn,Dyn)>>>'], List(Dyn))
Assign the function simpleSurface the type: Function(['baseColour:Dyn'], Tuple(Dyn,Float,Float,Dyn))
Assign the function bench_raytrace the type: Function(['loops:Dyn', 'width:Dyn', 'height:Dyn', 'filename:Dyn'], Void)
Assign the function main the type: Function([], Dyn)
"""

DEFAULT_WIDTH = 100
DEFAULT_HEIGHT = 100
EPSILON = 0.00001
ZERO = (0, 0, 0)#vector(0, 0, 0)
RIGHT = (1, 0, 0)#vector(1, 0, 0)
UP = (0, 1, 0)#vector(0, 1, 0)
OUT = (0, 0, 1)#vector(0, 0, 1)

def vector(initx, inity, initz)->Tuple(Dyn,Dyn,Dyn):
    return (initx, inity, initz)

def dot(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Float,Float,Float))->float:
    #other.mustBeVector()
    (x,y,z) = vec
    (x1, y1, z1) = other
    return (x * x1 * 1.0) + (y * y1 * 1.0) + (z * z1 * 1.0)

def magnitude(vec:Tuple(Dyn,Dyn,Dyn))->float:
    return (math.sqrt(dot(vec, vec)) + 0.0)

def add(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))-> Tuple(Dyn,Dyn,Dyn):
    (x,y,z) = vec
    (x1,y1,z1) = other
    return (x+x1, y+y1, z+z1)
    #if other.isPoint():
        #return Point(self.x + other.x, self.y + other.y, self.z + other.z)
    #else:
        #return Vector(self.x + other.x, self.y + other.y, self.z + other.z)

def sub(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):
    #other.mustBeVector()
    (x,y,z) = vec
    (x1,y1,z1) = other
    return vector(x - x1, y - y1, z - z1)

def scale(vec:Tuple(Dyn,Dyn,Dyn), factor)->Tuple(Dyn,Dyn,Dyn):
    x,y,z = vec
    return (factor * x, factor * y, factor * z)

def cross(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):
    #other.mustBeVector()
    (x,y,z) = vec
    (x1,y1,z1) = other
    return (y * z1 - z * y1,
                      z * x1 - x * z1,
                      x * y1 - y * x1)

def normalized(vec:Tuple(Dyn,Dyn,Dyn))-> Tuple(Dyn,Dyn,Dyn):
    return scale(vec, 1.0 / magnitude(vec))

def negated(vec:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):
    return scale(vec, -1)

def eq(vec:Tuple(Dyn,Dyn,Dyn), other:Tuple(Dyn,Dyn,Dyn))->Bool:
    x,y,z = vec
    x1,y1,z1 = other
    return (x == x1) and (y == y1) and (z == z1)

def reflectThrough(vec:Tuple(Dyn,Dyn,Dyn), normal:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):
    d = scale(normal, dot(vec, normal))
    return sub(vec, scale(d,2))

def sphere(centre, radius)->Tuple(Dyn,Dyn):
    #centre.mustBePoint()
    #self.centre = centre
    #self.radius = radius
    return (centre, radius)
def ray(point, vect:Tuple(Dyn,Dyn,Dyn)):
    #self.point = point
    #self.vector = vector.normalized()
    return (point, normalized(vect))

def pointAtTime(ray:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn)), t)->Tuple(Dyn,Dyn,Dyn):
    point, vector = ray
    return add(point, scale(vector, t))

def intersectionTime(s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn), rayy:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn))):
    (centre, radius) = s
    (point, vect) = rayy
    cp = sub(centre, point)
    v = dot(cp, vect)
    discriminant = (radius * radius) - (dot(cp, cp) - v * v)
    if discriminant < 0:
        return None
    else:
        return v - math.sqrt(discriminant)

def normalAt(s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn), p:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):
    (centre, radius) = s
    return normalized((sub(p,  centre)))

def halfspace(point, normal:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn, Tuple(Dyn,Dyn,Dyn)):
    #self.point = point
    #self.normal = normal.normalized()
    return (point, normalized(normal))
def intersectionTime(s, rayy:Tuple(Tuple(Dyn,Dyn,Dyn),Tuple(Dyn,Dyn,Dyn))):
    (centre, radius) = s
    (point, vect) = rayy
    cp = sub(centre, point)
    v = dot(cp, vect)
    discriminant = (radius * radius) - (dot(cp, cp) - v * v)
    if discriminant < 0:
        return None
    else:
        return v - math.sqrt(discriminant)

def normalAt(s:Tuple(Tuple(Dyn,Dyn,Dyn),Dyn), p:Tuple(Dyn,Dyn,Dyn))->Tuple(Dyn,Dyn,Dyn):
    (centre, radius) = s
    return normalized((sub(p,  centre)))

def canvas(width, height)->Tuple(Dyn,Dyn,int):
    byts = array.array('B', [0] * (width * height * 3))
    #byts = [0] * (width * height * 3)
    for i in xrange(width * height):
        #0
        byts[i * 3 + 2] = 255
    #self.width = width
    #self.height = height
    return (byts, width, height)

def plot(canv:Tuple(Dyn,Int,Int), x:int, y:int, r:int, g:int, b:int):
    (byts, width, height) = canv
    i = ((height - y - 1) * width + x) * 3
    byts[i] = max(0, min(255, int(r * 255)))
    byts[i + 1] = max(0, min(255, int(g * 255)))
    byts[i + 2] = max(0, min(255, int(b * 255)))
    return None

def firstIntersection(intersections):
    result = None
    for i in intersections:
        candidateT = i[1]
        if candidateT is not None and candidateT > -EPSILON:
            if result is None or candidateT < result[1]:
                result = i
    return result

def scene()->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Int):
    objects = []
    lightPoints = []
    position = vector(0.0, 1.8, 10.0)
    lookingAt = (0.0,0.0,0.0)#Point.ZERO
    fieldOfView = 45
    recursionDepth = 0
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

def moveTo(sc, p)->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    position = p
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

def lookAt(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), p):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    lookingAt = p
    return (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

def addObject(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), object, surface)-> Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    objs = [] + objects
    objs.append((object, surface))
    return (objs, lightPoints, position, lookingAt, fieldOfView, recursionDepth)

def addLight(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), p)->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    lps = [] + lightPoints
    lps.append(p)
    return (objects, lps, position, lookingAt, fieldOfView, recursionDepth)

def addColours(a, scale, b)->Tuple(Dyn,Dyn,Dyn):
    return (a[0] + scale * b[0],
            a[1] + scale * b[1],
            a[2] + scale * b[2])

def baseColourAt(ss, p):
    (baseColour, specularCoefficient, lambertCoefficient, ambientCoefficient) = ss
    return baseColour

def render(sc, canvas1):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    fovRadians = math.pi * (fieldOfView / 2.0) / 180.0
    halfWidth = math.tan(fovRadians)
    halfHeight = 0.75 * halfWidth
    width = halfWidth * 2
    height = halfHeight * 2
    bytes, w, h = canvas1
    pixelWidth = width / (w - 1)
    pixelHeight = height / (h - 1)

    eye = ray(position, sub(lookingAt, position))
    p, v = eye    
    vpRight = normalized(cross(v, UP))
    vpUp = normalized(cross(vpRight, v))

    for y in xrange(int(height)):
        for x in xrange(int(width)):
            xcomp = scale(vpRight, x * pixelWidth - halfWidth)
            ycomp = scale(vpUp, y * pixelHeight - halfHeight)
            r = ray(p, add(add(v, xcomp), ycomp))
            #colour = rayColour(sc, r)
            #plot(canvas1, x, y, *colour)

    return None

def lightIsVisible(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), l, p):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    for (o, s) in objects:
        t = intersectionTime(o, ray(p, sub(l, p)))
        if t is not None and t > EPSILON:
            return False
    return True

def visibleLights(sc:Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), p):
    (objects, lightPoints, position, lookingAt, fieldOfView, recursionDepth) = sc
    result = []
    for l in lightPoints:
        if lightIsVisible(sc, l, p):
            result.append(l)
    return result

def simpleSurface(baseColour)->Tuple(Dyn,Float,Float,Dyn):
    #baseColour = kwargs.get('baseColour', (1, 1, 1))
    specularCoefficient = 0.2
    lambertCoefficient =  0.6
    ambientCoefficient = 1.0 - specularCoefficient - lambertCoefficient
    return (baseColour, specularCoefficient, lambertCoefficient, ambientCoefficient)

def bench_raytrace(loops, width, height, filename):
    range_it = xrange(loops)
    #t0 = perf.perf_counter()

    for i in range_it:
        #canvas1 = canvas(width, height)
        s = scene()
        addLight(s, vector(30, 30, 10))
        addLight(s, vector(-10, 100, 30))
        lookAt(s, vector(0, 3, 0))
        addObject(s, sphere(vector(1, 3, -10), 2),
                    simpleSurface((1, 1, 0)))
        for y in xrange(6):
            addObject(s,sphere(vector(-3 - y * 0.4, 2.3, -5), 0.4),
                        simpleSurface((y / 6.0, 1 - y / 6.0, 0.5)))
            scale(normalized(vector(10,23,19)), y * 11)
        #s.addObject(Halfspace(Point(0, 0, 0), Vector.UP),
        #            CheckerboardSurface())
        #render(s,canvas1)
    return None



    #dt = perf.perf_counter() - t0

    #if filename:
    #    canvas.write_ppm(filename)
    #return dt
def main():
    t0 = time.time()
    bench_raytrace(100, DEFAULT_WIDTH, DEFAULT_HEIGHT, "raytrace.ppm")
    t1 = time.time()
    print(t1-t0)

main()

