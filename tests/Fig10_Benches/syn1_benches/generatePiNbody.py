import random
header = """
from six.moves import xrange
import pdb
from itertools import islice
import time
import itertools
from six.moves import map as imap
__contact__ = "collinwinter@google.com (Collin Winter)"
DEFAULT_ITERATIONS = 10000
DEFAULT_REFERENCE = 'sun'

"""

#Assign the function combinations the type: Function(['l:Dyn'], List(Dyn))
#Assign the function advance the type: Function(['dt:Choice a<Dyn, Float>', 'n:Dyn', 'bodies:Dyn', 'pairs:Dyn'], Choice a<Dyn, Float>)
#Assign the function report_energy the type: Function(['bodies:Dyn', 'pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))', 'e:Choice g<Dyn, Float>'], Choice g<Dyn, Float>)
#Assign the function offset_momentum the type: Function(['ref:Tuple(Dyn,Dyn,Dyn)', 'bodies:Dyn', 'px:Choice k<Dyn, Float>', 'py:Choice l<Dyn, Float>', 'pz:Choice m<Dyn, Float>'], Void)
#Assign the function bench_nbody the type: Function(['loops:Dyn', 'reference:Dyn', 'iterations:Dyn'], Void)
#Assign the function main the type: Function([], Void)

combinations_headers = ["def combinations(l):","def combinations(l)->List(Dyn):", "def combinations(l:List(Dyn))->List(Dyn):"]
combinations_body = """
    result = []
    for x in xrange((len(l) - 1)):
        ls = islice(l,x+1,len(l))#l[x + 1:]
        for y in ls:
            result.append((l[x], y))
    return result




PI = 3.14159265358979323
SOLAR_MASS = 4 * PI * PI
DAYS_PER_YEAR = 365.24
#pdb.set_trace()
BODIES = {
    'sun': ([0.0, 0.0, 0.0], [0.0, 0.0, 0.0], SOLAR_MASS),

    'jupiter': ([4.84143144246472090e+00,
                 -1.16032004402742839e+00,
                 -1.03622044471123109e-01],
                [1.66007664274403694e-03 * DAYS_PER_YEAR,
                 7.69901118419740425e-03 * DAYS_PER_YEAR,
                 -6.90460016972063023e-05 * DAYS_PER_YEAR],
                9.54791938424326609e-04 * SOLAR_MASS),

    'saturn': ([8.34336671824457987e+00,
                4.12479856412430479e+00,
                -4.03523417114321381e-01],
               [(-2.76742510726862411e-03 * DAYS_PER_YEAR),
                (4.99852801234917238e-03 * DAYS_PER_YEAR),
                (2.30417297573763929e-05 * DAYS_PER_YEAR)],
               (2.85885980666130812e-04 * SOLAR_MASS)),

    'uranus': ([1.28943695621391310e+01,
                -1.51111514016986312e+01,
                -2.23307578892655734e-01],
               [2.96460137564761618e-03 * DAYS_PER_YEAR,
                2.37847173959480950e-03 * DAYS_PER_YEAR,
                -2.96589568540237556e-05 * DAYS_PER_YEAR],
               4.36624404335156298e-05 * SOLAR_MASS),

    'neptune': ([1.53796971148509165e+01,
                 -2.59193146099879641e+01,
                 1.79258772950371181e-01],
                [2.68067772490389322e-03 * DAYS_PER_YEAR,
                 1.62824170038242295e-03 * DAYS_PER_YEAR,
                 -9.51592254519715870e-05 * DAYS_PER_YEAR],
                5.15138902046611451e-05 * SOLAR_MASS)}


SYSTEM = list(BODIES.values())
PAIRS = combinations(SYSTEM)

"""

advance_headers= ["def advance(dt, n, bodies, pairs):",
                 "def advance(dt:float, n, bodies, pairs):",
                 "def advance(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                 "def advance(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                 "def advance(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                 "def advance(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                 "def advance(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                 "def advance(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]

advance_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""


report_energy_headers = ["def report_energy(bodies, pairs, e):",
                       "def report_energy(bodies, pairs, e:float)->float:",
                       "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                       "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                       "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                       "def report_energy(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                       "def report_energy(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                       "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                       "def report_energy(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]
report_energy_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum_headers = ["def offset_momentum(ref, bodies, px, py, pz):",
                           "def offset_momentum(ref, bodies, px:float, py:float, pz:float):",
                           "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                           "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                           "def offset_momentum(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                           "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                           "def offset_momentum(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None
    #ref[1] = a #NOTE: Guarded messes up object identity
    #v[0] = a[0]
    #v[1] = a[1]
    #v[2] = a[2]

"""
    
    
bench_nbody_headers = ["def bench_nbody(loops, reference, iterations):"]
bench_nbody_body = """
    # Set up global state
    offset_momentum(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy(SYSTEM, PAIRS, 0.0)
        advance(0.01, iterations, SYSTEM, PAIRS)
        report_energy(SYSTEM, PAIRS, 0.0)
    return None
"""

advance1_headers = ["def advance1(dt, n, bodies, pairs):",
                    "def advance1(dt:float, n, bodies, pairs):",
                    "def advance1(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                    "def advance1(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs):",
                    "def advance1(dt:float, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                    "def advance1(dt, n, bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                    "def advance1(dt, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):",
                    "def advance1(dt:float, n, bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn)))):"]
advance1_body = """
    for i in xrange(n):
        for (([x1, y1, z1], v1, m1),
             ([x2, y2, z2], v2, m2)) in pairs:            
            dx = 0.0 + x1 - x2
            dy = (0.0 + y1 - y2)
            dz = (0.0 + z1 - z2)
            mag = (1.0 * dt * (((1.0 * dx * dx) + (1.0 * dy * dy) + (1.0 * dz * dz)) ** (-1.5)))
            b1m = (m1 * mag)
            b2m = (m2 * mag)
            v1[0] -= (dx * b2m)
            v1[1] -= (dy *  b2m)
            v1[2] -= (dz * b2m)
            v2[0] += (dx * b1m)
            v2[1] += (dy * b1m)
            v2[2] += (dz * b1m)
        for (r, [vx, vy, vz], m) in bodies:
            r[0] += (dt * vx)
            r[1] += (dt * vy)
            r[2] += (dt * vz)

"""

report_energy1_headers = ["def report_energy1(bodies, pairs, e):",
                          "def report_energy1(bodies, pairs, e:float)->float:",
                          "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                          "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e:float)->float:",
                          "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs, e):",
                          "def report_energy1(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                          "def report_energy1(bodies, pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):",
                          "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e:float)->float:",
                          "def report_energy1(bodies:List(Tuple(Dyn,Dyn,Dyn)), pairs:List(Tuple(Dyn,Tuple(Dyn,Dyn,Dyn))), e):"]
report_energy1_body = """
    for (((x1, y1, z1), v1, m1),
         ((x2, y2, z2), v2, m2)) in pairs:
        dx = (x1 - x2)
        dy = (y1 - y2)
        dz = (z1 - z2)
        e -= ((m1 * m2) / ((((dx * dx) + (dy * dy)) + (dz * dz))) ** 0.5)
    for (r, [vx, vy, vz], m) in bodies:
        e += ((m * ((((vx * vx) + (vy * vy)) + (vz * vz)))) / 2.0)
    return e

"""

offset_momentum1_headers= ["def offset_momentum1(ref, bodies, px, py, pz):",
                           "def offset_momentum1(ref, bodies, px:float, py:float, pz:float):",
                           "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies, px:float, py:float, pz:float):",
                           "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies, px, py, pz):",
                           "def offset_momentum1(ref, bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                           "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):",
                           "def offset_momentum1(ref:Tuple(Dyn,Dyn,Dyn), bodies:List(Tuple(Dyn,Dyn,Dyn)), px:float, py:float, pz:float):"]

offset_momentum1_body = """
    for (r, [vx, vy, vz], m) in bodies:
        px -= (vx * m)
        py -= (vy * m)
        pz -= (vz * m)
    (r, v, m) = ref
    a = [((0.0 + px) / m),((0.0 + py) / m),((0.0 + pz) / m)]
    return None
    #ref[1] = a #NOTE: Guarded messes up object identity
    #v[0] = a[0]
    #v[1] = a[1]
    #v[2] = a[2]

"""
    
    
#def bench_nbody(loops, reference, iterations):
bench_nbody1_headers = ["def bench_nbody1(loops, reference, iterations):"]
bench_nbody1_body = """
    # Set up global state
    offset_momentum1(BODIES[reference], SYSTEM, 0.0, 0.0, 0.0)

    range_it = xrange(loops)

    for _ in range_it:
        report_energy1(SYSTEM, PAIRS, 0.0)
        advance1(0.01, iterations, SYSTEM, PAIRS)
        report_energy1(SYSTEM, PAIRS, 0.0)
    return None




DEFAULT_DIGITS = 2000
icount = itertools.count
islice = itertools.islice

"""

gen_x_headers = ["def gen_x():"]
gen_x_body = """
    #return imap(lambda k: (k, iadd(imult(4, k), 2), 0, iadd(imult(2, k), 1)), icount(1))
    return imap(lambda k: (k, 4 * k + 2, 0, 2 * k + 1), icount(1))

"""

compose_headers = ["def compose(a,b):",
                   "def compose(a, b)->Tuple(int,int,int,int):",
                   "def compose(a:Tuple(int,int,int,int), b)->Tuple(int,int,int,int):",
                   "def compose(a, b:Tuple(int,int,int,int))->Tuple(int,int,int,int):",
                   "def compose(a:Tuple(int,int,int,int), b:Tuple(int,int,int,int))->Tuple(int,int,int,int):"]

compose_body = """
    aq, ar, as_, at = a
    bq, br, bs, bt = b
    return (aq * bq + 0,
            aq * br + ar * bt + 0,
            as_ * bq + at * bs + 0,
            as_ * br + at * bt + 0)
    #return (imult(aq, bq),
    #        iadd(imult(aq, br), imult(ar, bt)),
    #        iadd(imult(as_,  bq), imult(at, bs)),
    #        iadd(imult(as_, br), imult(at, bt)))

"""

extract_headers = ["def extract(z,j):",
                   "def extract(z, j)->int:",
                   "def extract(z:Tuple(int,int,int,int), j) -> int:",
                   "def extract(z, j:int) -> int:",
                   "def extract(z:Tuple(int,int,int,int), j:int) -> int:"]
extract_body = """
    q, r, s, t = z
    #return idiv((iadd(imult(q, j), r)), (iadd(imult(s, j),  t)))
    return (q * j + r + 0) // (s * j + t + 0)

"""


#def gen_pi_digits():
gen_pi_digits_headers = ["def gen_pi_digits():"]
gen_pi_digits_body = """
    z = (1, 0, 0, 1)
    x = gen_x()
    while True:
        y = extract(z, 3)
        while y != extract(z, 4):
            z = compose(z, next(x))
            y = extract(z, 3)
        #z = compose((10, imult(-10, y), 0, 1), z)
        z = compose((10, (-10 * y), 0, 1), z)
        yield y

"""

calc_ndigits_headers = ["def calc_ndigits(n):"]
#def calc_ndigits(n) -> List(Dyn):
calc_ndigits_body = """
    return list(islice(gen_pi_digits(), n))

"""



#def add_cmdline_args(cmd, args):
#    cmd.extend(("--digits", str(args.digits)))
gen_x1_headers = ["def gen_x1():"]
gen_x1_body = """
    #return imap(lambda k: (k, iadd(imult(4, k), 2), 0, iadd(imult(2, k), 1)), icount(1))
    return imap(lambda k: (k, 4 * k + 2, 0, 2 * k + 1), icount(1))

"""

compose1_headers = ["def compose1(a,b):",
                    "def compose1(a, b)->Tuple(int,int,int,int):",
                    "def compose1(a:Tuple(int,int,int,int), b)->Tuple(int,int,int,int):",
                    "def compose1(a, b:Tuple(int,int,int,int))->Tuple(int,int,int,int):",
                    "def compose1(a:Tuple(int,int,int,int), b:Tuple(int,int,int,int))->Tuple(int,int,int,int):"]
compose1_body = """
    aq, ar, as_, at = a
    bq, br, bs, bt = b
    return (aq * bq + 0,
            aq * br + ar * bt + 0,
            as_ * bq + at * bs + 0,
            as_ * br + at * bt + 0)
    #return (imult(aq, bq),
    #        iadd(imult(aq, br), imult(ar, bt)),
    #        iadd(imult(as_,  bq), imult(at, bs)),
    #        iadd(imult(as_, br), imult(at, bt)))

"""

extract1_headers = ["def extract1(z,j):",
                    "def extract1(z, j)->int:",
                    "def extract1(z:Tuple(int,int,int,int), j) -> int:",
                    "def extract1(z, j:int) -> int:",
                    "def extract1(z:Tuple(int,int,int,int), j:int) -> int:"]
extract1_body = """
    q, r, s, t = z
    #return idiv((iadd(imult(q, j), r)), (iadd(imult(s, j),  t)))
    return (q * j + r + 0) // (s * j + t + 0)

"""


#def gen_pi_digits():
gen_pi_digits1_headers = ["def gen_pi_digits1():"]
gen_pi_digits1_body = """
    z = (1, 0, 0, 1)
    x = gen_x1()
    while True:
        y = extract(z, 3)
        while y != extract1(z, 4):
            z = compose1(z, next(x))
            y = extract1(z, 3)
        #z = compose((10, imult(-10, y), 0, 1), z)
        z = compose1((10, (-10 * y), 0, 1), z)
        yield y

"""

calc_ndigits1_headers = ["def calc_ndigits1(n):","def calc_ndigits1(n) -> List(Dyn):"]
calc_ndigits1_body = """
    return list(islice(gen_pi_digits1(), n))

"""


main_headers = ["def main():"]
main_body = """
    bench_nbody(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)
    #bench_nbody1(1,DEFAULT_REFERENCE,DEFAULT_ITERATIONS)
    calc_ndigits(100)
    calc_ndigits1(100)


t0 = time.time()
main()
t1 = time.time()
print(t1-t0)

"""

for i in range(100):
    with open("pn" + str(i) + ".py", "w+") as f:
        combinations = combinations_headers[random.randrange(0,len(combinations_headers))] + combinations_body
        advance = advance_headers[random.randrange(0,len(advance_headers))] + advance_body
        report_energy = report_energy_headers[random.randrange(0,len(report_energy_headers))] + report_energy_body
        offset_momentum = offset_momentum_headers[random.randrange(0,len(offset_momentum_headers))] + offset_momentum_body
        bench_nbody = bench_nbody_headers[random.randrange(0,len(bench_nbody_headers))] + bench_nbody_body
        advance1 = advance1_headers[random.randrange(0,len(advance1_headers))] + advance1_body
        report_energy1 = report_energy1_headers[random.randrange(0,len(report_energy1_headers))] + report_energy1_body
        offset_momentum1 = offset_momentum1_headers[random.randrange(0,len(offset_momentum1_headers))] + offset_momentum1_body
        bench_nbody1 = bench_nbody1_headers[random.randrange(0,len(bench_nbody1_headers))] + bench_nbody1_body
        gen_x = gen_x_headers[random.randrange(0,len(gen_x_headers))] + gen_x_body
        compose = compose_headers[random.randrange(0,len(compose_headers))] + compose_body
        extract = extract_headers[random.randrange(0,len(extract_headers))] + extract_body
        gen_pi_digits = gen_pi_digits_headers[random.randrange(0,len(gen_pi_digits_headers))] + gen_pi_digits_body
        calc_ndigits = calc_ndigits_headers[random.randrange(0,len(calc_ndigits_headers))] + calc_ndigits_body
        gen_x1 = gen_x1_headers[random.randrange(0,len(gen_x1_headers))] + gen_x1_body
        compose1 = compose1_headers[random.randrange(0,len(compose1_headers))] + compose1_body
        extract1 = extract1_headers[random.randrange(0,len(extract1_headers))] + extract1_body
        gen_pi_digits1 = gen_pi_digits1_headers[random.randrange(0,len(gen_pi_digits1_headers))] + gen_pi_digits1_body
        calc_ndigits1 = calc_ndigits1_headers[random.randrange(0,len(calc_ndigits1_headers))] + calc_ndigits1_body
        main = main_headers[random.randrange(0,len(main_headers))] + main_body
        out = header + combinations + advance + report_energy + offset_momentum + bench_nbody + advance1 + report_energy1 + offset_momentum1 + bench_nbody1 + gen_x + compose + extract + gen_pi_digits + calc_ndigits + gen_x1 + compose1 + extract1 + gen_pi_digits1 + calc_ndigits1 + main
        f.write(out)
