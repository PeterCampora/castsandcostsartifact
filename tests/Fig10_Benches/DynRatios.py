with open("../benchmarks/log.txt", 'r') as d:
    lines = d.readlines()
    fftDyn = eval(lines[1])
    floatDyn = eval(lines[3])
    metDyn = eval(lines[5])
    mcDyn = eval(lines[7])
    nbodyDyn = eval(lines[9])
    piDyn = eval(lines[11])
    raytraceDyn = eval(lines[13])
    SORDyn = eval(lines[15])
    snDyn = eval(lines[17])
    syn1Dyn = eval(lines[19])
    syn2Dyn = eval(lines[21])
    syn3Dyn = eval(lines[23])

    with open("fft_benches/log.txt") as fft:
        fft_lines = fft.readlines()
        fftPred = eval(fft_lines[1])
        print("sm(FFT)")
        print(fftDyn / fftPred)

    with open("float_benches/log.txt") as flt:
        float_lines = flt.readlines()
        floatPred = eval(float_lines[1])
        print("float")
        print(floatDyn / floatPred)

    with open("meteor_contest_benches/log.txt") as met:
        met_lines = met.readlines()
        metPred = eval(met_lines[1])
        print("meteor_contest")
        print(metDyn / metPred)

    with open("monte_carlo_benches/log.txt") as mc:
        mc_lines = mc.readlines()
        mcPred = eval(mc_lines[1])
        print("sm(MC)")
        print(mcDyn / mcPred)

    with open("nbody_benches/log.txt") as nbody:
        nbody_lines = nbody.readlines()
        nbodyPred = eval(nbody_lines[1])
        print("nbody")
        print(nbodyDyn / nbodyPred)

    with open("pidigits_benches/log.txt") as pi:
        pi_lines = pi.readlines()
        piPred = eval(pi_lines[1])
        print("pidigits")
        print(piDyn / piPred)

    with open("raytrace_benches/log.txt") as raytrace:
        raytrace_lines = raytrace.readlines()
        raytracePred = eval(raytrace_lines[1])
        print("raytrace")
        print(raytraceDyn / raytracePred)

    with open("sci_mark_benches/log.txt") as sm:
        sm_lines = sm.readlines()
        smPred = eval(sm_lines[1])
        print("sm(SOR)")
        print(SORDyn / smPred)

    with open("spectral_norm_benches/log.txt") as sn:
        sn_lines = sn.readlines()
        snPred = eval(sn_lines[1])
        print("spectral_norm")
        print(snDyn / snPred)

    with open("syn1_benches/log.txt") as s1:
        s1_lines = s1.readlines()
        s1Pred = eval(s1_lines[1])
        print("syn_1")
        print(syn1Dyn / s1Pred)

    with open("syn2_benches/log.txt") as s2:
        s2_lines = s2.readlines()
        s2Pred = eval(s2_lines[1])
        print("syn_2")
        print(syn1Dyn / s2Pred)

    with open("syn3_benches/log.txt") as s3:
        s3_lines = s3.readlines()
        s3Pred = eval(s3_lines[1])
        print("syn_3")
        print(syn3Dyn / s3Pred)

    
        
