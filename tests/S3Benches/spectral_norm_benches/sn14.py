
#import perf
import time
from six.moves import xrange, zip as izip

DEFAULT_N = 130

def eval_A(i:float, j:float)->float:
    return 1.0 / ((i + j) * (i + j + 1.0) // 2.0 + i + 1.0)

def part_A_times_u(i_u)->float:
    i, u = i_u
    partial_sum = 0.0
    for j, u_j in enumerate(u):
        #partial_sum = add(partial_sum, mult(eval_A(i, j), u_j))
        partial_sum = partial_sum + eval_A(i,j) * u_j
        #partial_sum = eval_A(i,j)
    return partial_sum

def part_At_times_u(i_u) ->float:
    i, u = i_u
    partial_sum = 0.0
    for j, u_j in enumerate(u):
        partial_sum = partial_sum + eval_A(j, i) * u_j
        #partial_sum = add(partial_sum, mult(eval_A(j, i), u_j))
    return partial_sum


def my_bench(loops):
    range_it = xrange(loops)
    for _ in range_it:
        u = [1.0] * DEFAULT_N

        for x in xrange(10):
            part_A_times_u((x,u))
            part_At_times_u((x,u))
            #eval_times_u(part_A_times_u,u)
    return loops

def main():
    sum = 0
    #for _ in range(10):
    t0 = time.time()
    #bench_spectral_norm(1)
    my_bench(100)
    t1 = time.time()
    sum += t1-t0
    
    print(sum)

main()
