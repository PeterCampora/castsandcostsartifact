#!/usr/bin/env bash
MAXCOUNT=5
count=1


while [ "$count" -le $MAXCOUNT ]      # Generate 10 ($MAXCOUNT) random integers.
do
    number=$RANDOM
    let "number %= 95"
    let "number += 2"
    fName="fft"$number
    cp ../Fig10_Benches/fft_benches/"$fName".py fft_benches
    let "count += 1"  # Increment count.
done

count=1
while [ "$count" -le $MAXCOUNT ]      # Generate 10 ($MAXCOUNT) random integers.
do
    number=$RANDOM
    let "number %= 16"
    let "number += 1"
    fName="float"$number
    cp ../Fig10_Benches/float_benches/"$fName".py float_benches
    let "count += 1"  # Increment count.
done

count=1
while [ "$count" -le $MAXCOUNT ]      # Generate 10 ($MAXCOUNT) random integers.
do
    number=$RANDOM
    let "number %= 100"
    let "number += 1"
    fName="met"$number
    cp ../Fig10_Benches/meteor_contest_benches/"$fName".py meteor_contest_benches
    let "count += 1"  # Increment count.
done


count=1
while [ "$count" -le $MAXCOUNT ]      # Generate 10 ($MAXCOUNT) random integers.
do
    number=$RANDOM
    let "number %= 16"
    let "number += 1"
    fName="monte_carlo"$number
    cp ../Fig10_Benches/monte_carlo_benches/"$fName".py monte_carlo_benches
    let "count += 1"  # Increment count.
done

count=1
while [ "$count" -le $MAXCOUNT ]      # Generate 10 ($MAXCOUNT) random integers.
do
    number=$RANDOM
    let "number %= 100"
    let "number += 1"
    fName="nbody"$number
    cp ../Fig10_Benches/nbody_benches/"$fName".py nbody_benches
    let "count += 1"  # Increment count.
done

count=1
while [ "$count" -le $MAXCOUNT ]      # Generate 10 ($MAXCOUNT) random integers.
do
    number=$RANDOM
    let "number %= 16"
    let "number += 1"
    fName="pi"$number
    cp ../Fig10_Benches/pidigits_benches/"$fName".py pidigits_benches
    let "count += 1"  # Increment count.
done

count=1
while [ "$count" -le $MAXCOUNT ]      # Generate 10 ($MAXCOUNT) random integers.
do
    number=$RANDOM
    let "number %= 100"
    let "number += 1"
    fName="raytrace"$number
    cp ../Fig10_Benches/raytrace_benches/"$fName".py raytrace_benches
    let "count += 1"  # Increment count.
done

count=1
while [ "$count" -le $MAXCOUNT ]      # Generate 10 ($MAXCOUNT) random integers.
do
    number=$RANDOM
    let "number %= 100"
    let "number += 1"
    fName="SOR"$number
    cp ../Fig10_Benches/sci_mark_benches/"$fName".py sci_mark_benches
    let "count += 1"  # Increment count.
done


count=1
while [ "$count" -le $MAXCOUNT ]      # Generate 10 ($MAXCOUNT) random integers.
do
    number=$RANDOM
    let "number %= 16"
    let "number += 1"
    fName="sn"$number
    cp ../Fig10_Benches/spectral_norm_benches/"$fName".py spectral_norm_benches
    let "count += 1"  # Increment count.
done
