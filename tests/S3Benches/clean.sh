rm fft_benches/*.py
cp ../benchmarks/fft.py fft_benches
rm float_benches/*.py
cp ../benchmarks/float.py float_benches
rm meteor_contest_benches/*.py
cp ../benchmarks/meteor_contest.py meteor_contest_benches
rm monte_carlo_benches/*.py
cp ../benchmarks/monte_carlo.py monte_carlo_benches
rm nbody_benches/*.py
cp ../benchmarks/nbody.py nbody_benches
rm pidigits_benches/*.py
cp ../benchmarks/pidigits.py pidigits_benches
rm raytrace_benches/*.py
cp ../benchmarks/raytrace.py raytrace_benches
rm sci_mark_benches/*.py
cp ../benchmarks/sci_markSOR.py sci_mark_benches
rm spectral_norm_benches/*.py
cp ../benchmarks/spectral_norm.py spectral_norm_benches
