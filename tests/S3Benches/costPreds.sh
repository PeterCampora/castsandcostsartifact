echo "float"
cd float_benches/pred
sh costBenches.sh
cd ../..
echo ""

echo "meteor_contest"
cd meteor_contest_benches/pred
sh costBenches.sh
cd ../..
echo ""

echo "nbody"
cd nbody_benches/pred
sh costBenches.sh
cd ../..
echo ""

echo "raytrace"
cd raytrace_benches/pred
sh costBenches.sh
cd ../..
echo ""

echo "fft"
cd fft_benches/pred
sh costBenches.sh
cd ../..
echo ""


echo "monte_carlo"
cd monte_carlo_benches/pred
sh costBenches.sh
cd ../..
echo ""

echo "spectral_norm"
cd spectral_norm_benches/pred
sh costBenches.sh
cd ../..


