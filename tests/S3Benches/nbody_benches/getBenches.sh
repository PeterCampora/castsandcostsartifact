MAXCOUNT=5
count=1
while [ "$count" -le $MAXCOUNT ]      # Generate 10 ($MAXCOUNT) random integers.
do
    number=$RANDOM
    let "number %= 100"
    let "number += 1"
    fName="nbody"$number
    cp ../../Fig10_Benches/nbody_benches/"$fName".py ./
    let "count += 1"  # Increment count.
done
