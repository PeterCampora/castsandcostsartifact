
from array import array
import math

import perf
from six.moves import xrange
import time

#Assign the function initialize the type: Function(['tup:Tuple(Dyn,Dyn,Dyn,Int,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn)', 'seed:Dyn'], Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn))
#Assign the function rand the type: Function(['seed:Dyn'], Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn))
#Assign the function nextDouble the type: Function(['tup:Tuple(Dyn,Dyn,Dyn,Dyn,Float,Float,Dyn,Float,Bool,Dyn,Dyn,Dyn,Dyn)'], Tuple(Dyn,Float))
#Assign the function MonteCarlo the type: Function(['Num_samples:Choice c<Dyn, Float>'], Float)
#Assign the function main the type: Function([], Void)


MDIG = 32
ONE = 1
m1 = (ONE << (MDIG - 2)) + ((ONE << (MDIG - 2)) - ONE)
m2 = ONE << MDIG // 2
dm1 = 1.0 / float(m1)

def initialize(tup:Tuple(Dyn,Dyn,Dyn,Int,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn), seed):
    (MDIG, ONE, m1, m2, dm1, left, right, width, haveRange, m, mySeed,i,j) = tup
    mySeed = (seed+0.0)
    seed = abs(seed)
    jseed = min(seed, tup[2])
    if (jseed % 2 == 0):
        jseed -= 1
    k0 = 9069 % m2
    k1 = 9069 / m2
    j0 = jseed % m2
    j1 = jseed / m2
    m = array('d', [0]) * 17
    for iloop in xrange(17):
        jseed = j0 * k0
        j1 = (jseed / m2 + j0 * k1 + j1 * k0) % (m2 / 2)
        j0 = jseed % m2
        m[iloop] = j0 + m2 * j1
    i = 4
    j = 16
    return (MDIG, ONE, m1, m2, dm1, left, right, width, haveRange, m, mySeed,i,j)

def rand(seed:float)->Tuple(Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn,Dyn):
    left = 0.0
    right = 1.0
    width = 1.0
    haveRange = False
    m = []
    #0=MDIG,1=ONE,2=m1,3=m2,4=dm1,5=left,6=right,7=width,8=haveRange,9=m,10=seed,i=11,j=12
    tup = (MDIG, ONE, m1, m2, dm1, left, right, width, haveRange, m, seed+0.0,0,0)
    tup = initialize(tup, seed)    
    return tup

def nextDouble(tup:Tuple(Dyn,Dyn,Dyn,Dyn,Float,Float,Dyn,Float,Bool,Dyn,Dyn,Dyn,Dyn))->Tuple(Dyn,Float):
    (MDIG, ONE, m1, m2, dm1, left, right, width, haveRange, m, mySeed,i,j) = tup
    I, J, m = i,j,m
    k = m[I] - m[J]
    if (k < 0):
        k += m1
    m[J] = k
    
    if (I == 0):
        I = 16
    else:
        I -= 1
    i = I

    if (J == 0):
        J = 16
    else:
        J -= 1
    j = J
    tup = (MDIG, ONE, m1, m2, dm1, left, right, width, haveRange, m, mySeed,i,j) 
    if (haveRange):
        return (tup,left + dm1 * float(k) * width)    
    return (tup, dm1 * float(k))


def MonteCarlo(Num_samples:float):
    rnd = rand(113)
    under_curve = 0
    for count in xrange(Num_samples):
        rnd,x = nextDouble(rnd)
        rnd,y = nextDouble(rnd)
        if x * x + y * y <= 1.0:
            under_curve += 1
    return float(under_curve) / Num_samples * 4.0

def main():
    t0 = time.time()
    #for _ in range(20):
    MonteCarlo(100)
    t1 = time.time()
    print(t1-t0)
    return None

main()
