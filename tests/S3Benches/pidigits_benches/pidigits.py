import itertools
import time
from six.moves import map as imap


DEFAULT_DIGITS = 2000
icount = itertools.count
islice = itertools.islice


def gen_x():
    #return imap(lambda k: (k, iadd(imult(4, k), 2), 0, iadd(imult(2, k), 1)), icount(1))
    return imap(lambda k: (k, 4 * k + 2, 0, 2 * k + 1), icount(1))

def compose(a,b):
#def compose(a, b)->Tuple(int,int,int,int):
#def compose(a:Tuple(int,int,int,int), b)->Tuple(int,int,int,int):
#def compose(a, b:Tuple(int,int,int,int))->Tuple(int,int,int,int):
#def compose(a:Tuple(int,int,int,int), b:Tuple(int,int,int,int))->Tuple(int,int,int,int):
    aq, ar, as_, at = a
    bq, br, bs, bt = b
    return (aq * bq + 0,
            aq * br + ar * bt + 0,
            as_ * bq + at * bs + 0,
            as_ * br + at * bt + 0)
    #return (imult(aq, bq),
    #        iadd(imult(aq, br), imult(ar, bt)),
    #        iadd(imult(as_,  bq), imult(at, bs)),
    #        iadd(imult(as_, br), imult(at, bt)))


def extract(z, j):
#def extract(z, j)->int:
#def extract(z:Tuple(int,int,int,int), j) -> int:    
#def extract(z, j:int) -> int:
#def extract(z:Tuple(int,int,int,int), j:int) -> int:    
    q, r, s, t = z
    #return idiv((iadd(imult(q, j), r)), (iadd(imult(s, j),  t)))
    return (q * j + r + 0) // (s * j + t + 0)


#def gen_pi_digits():
def gen_pi_digits():
    z = (1, 0, 0, 1)
    x = gen_x()
    while True:
        y = extract(z, 3)
        while y != extract(z, 4):
            z = compose(z, next(x))
            y = extract(z, 3)
        #z = compose((10, imult(-10, y), 0, 1), z)
        z = compose((10, (-10 * y), 0, 1), z)
        yield y

def calc_ndigits(n):
#def calc_ndigits(n) -> List(Dyn):
    return list(islice(gen_pi_digits(), n))

def main():
    calc_ndigits(100)



#def add_cmdline_args(cmd, args):
#    cmd.extend(("--digits", str(args.digits)))

t0 = time.time()
main()
t1 = time.time()
print(t1-t0)
#if __name__ == "__main__":
#    runner = perf.Runner(add_cmdline_args=add_cmdline_args)
#
#    cmd = runner.argparser
#    cmd.add_argument("--digits", type=int, default=DEFAULT_DIGITS,
#                     help="Number of computed pi digits (default: %s)"
#                          % DEFAULT_DIGITS)
#
#    args = runner.parse_args()
#    runner.metadata['description'] = "Compute digits of pi."
#    runner.metadata['pidigits_ndigit'] = args.digits
#    runner.bench_func('pidigits', calc_ndigits, args.digits)
