echo "float times"
cd float_benches
cat log.txt
cd pred
echo "float Herder prediction times"
cat log.txt
echo ""
cd ../..

echo "meteor_contest times"
cd meteor_contest_benches
cat log.txt
cd pred
echo "meteor_contest Herder prediction times"
cat log.txt
echo ""
cd ../..

echo "nbody times"
cd nbody_benches
cat log.txt
cd pred
echo "nbody Herder prediction times"
cat log.txt
echo ""
cd ../..

echo "pidigits times"
cd pidigits_benches
cat log.txt
echo ""
cd ..

echo "raytrace times"
cd raytrace_benches
cat log.txt
cd pred
echo "raytrace Herder prediction times"
cat log.txt
echo ""
cd ../..

echo "fft times"
cd fft_benches
cat log.txt
cd pred
echo "fft Herder prediction times"
cat log.txt
echo ""
cd ../..

echo "monte_carlo times"
cd monte_carlo_benches
cat log.txt
cd pred
echo "monte_carlo Herder prediction times"
cat log.txt
echo ""
cd ../..

echo "sci_mark times"
cd sci_mark_benches
cat log.txt
echo ""
cd ..

echo "spectral_norm times"
cd spectral_norm_benches
cat log.txt
cd pred
echo "spectral_norm Herder prediction times"
cat log.txt
echo ""
cd ../..


