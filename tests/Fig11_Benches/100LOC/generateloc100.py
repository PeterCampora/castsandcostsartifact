head = """
import random
import time
from array import array
import math

import perf
from six.moves import xrange
"""

int_log2_headers = ["def int_log2(n):",
                     "def int_log2(n:Int):"]
int_log2_body = """
    k = 1
    log = 0
    while k < (n+0):
        k *= 2
        log += 1
    if n != 1 << log:
        raise Exception("FFT: Data length is not a power of 2: %s" % n)
    return (log)

"""
FFT_bitreverse_headers = ["def FFT_bitreverse(N, data):",
                          "def FFT_bitreverse(N:Int, data):"]
FFT_bitreverse_body = """
    n = N // 2
    nm1 = n - 1
    j = 0
    for i in range(nm1):
        ii = i << 1
        jj = j << 1
        k = n >> 1
        if i < j:
            tmp_real = data[ii]
            tmp_imag = data[ii + 1]
            data[ii] = data[jj]
            data[ii + 1] = data[jj + 1]
            data[jj] = tmp_real
            data[jj + 1] = tmp_imag
        while k <= j:
            j -= k
            k >>= 1
        j += k

"""
FFT_transform_internal_headers = ["def FFT_transform_internal(N, data, direction):",
                                  "def FFT_transform_internal(N:int, data, direction):",
                                  "def FFT_transform_internal(N, data, direction:float):",
                                  "def FFT_transform_internal(N:int, data, direction:float):"]
FFT_transform_internal_body = """
    n = N // 2
    bit = 0
    dual = 1
    if n == 1:
        return None

    logn = int_log2(n)
    if N == 0:
        return None
    FFT_bitreverse(N, data)

    # apply fft recursion
    # this loop executed int_log2(N) times
    bit = 0
    while bit < logn:
        w_real = 1.0
        w_imag = 0.0
        theta = 2.0 * direction * math.pi / (2.0 * float(dual))
        s = math.sin(theta)
        t = math.sin(theta / 2.0)
        s2 = 2.0 * t * t
        for b in range(0, n, 2 * dual):
            i = 2 * b
            j = 2 * (b + dual)
            wd_real = data[j]
            wd_imag = data[j + 1]
            data[j] = data[i] - wd_real
            data[j + 1] = data[i + 1] - wd_imag
            data[i] += wd_real
            data[i + 1] += wd_imag
        for a in xrange(1, dual):
            tmp_real = w_real - s * w_imag - s2 * w_real
            tmp_imag = w_imag + s * w_real - s2 * w_imag
            w_real = tmp_real
            w_imag = tmp_imag
            for b in range(0, n, 2 * dual):
                i = 2 * (b + a)
                j = 2 * (b + a + dual)
                z1_real = data[j]
                z1_imag = data[j + 1]
                wd_real = w_real * z1_real - w_imag * z1_imag
                wd_imag = w_real * z1_imag + w_imag * z1_real
                data[j] = data[i] - wd_real
                data[j + 1] = data[i + 1] - wd_imag
                data[i] += wd_real
                data[i + 1] += wd_imag
        bit += 1
        dual *= 2
        n = N // 2
    bit = 0
    dual = 1
    if n == 1:
        return None

    logn = int_log2(n)
    if N == 0:
        return None
    FFT_bitreverse(N, data)

    # apply fft recursion
    # this loop executed int_log2(N) times
    bit = 0
    while bit < logn:
        w_real = 1.0
        w_imag = 0.0
        theta = 2.0 * direction * math.pi / (2.0 * float(dual))
        s = math.sin(theta)
        t = math.sin(theta / 2.0)
        s2 = 2.0 * t * t
        for b in range(0, n, 2 * dual):
            i = 2 * b
            j = 2 * (b + dual)
            wd_real = data[j]
            wd_imag = data[j + 1]
            data[j] = data[i] - wd_real
            data[j + 1] = data[i + 1] - wd_imag
            data[i] += wd_real
            data[i + 1] += wd_imag
        for a in xrange(1, dual):
            tmp_real = w_real - s * w_imag - s2 * w_real
            tmp_imag = w_imag + s * w_real - s2 * w_imag
            w_real = tmp_real
            w_imag = tmp_imag
            for b in range(0, n, 2 * dual):
                i = 2 * (b + a)
                j = 2 * (b + a + dual)
                z1_real = data[j]
                z1_imag = data[j + 1]
                wd_real = w_real * z1_real - w_imag * z1_imag
                wd_imag = w_real * z1_imag + w_imag * z1_real
                data[j] = data[i] - wd_real
                data[j + 1] = data[i + 1] - wd_imag
                data[i] += wd_real
                data[i + 1] += wd_imag
        bit += 1
        dual *= 2
        n = N // 2
        return None

def main():
    twoN = 2 * N
    init_vec = array('d', [random.random() for i in range(twoN)]) #Random(7).RandomVector(twoN)
    FFT_transform_internal(twoN, initvec, 1)

"""
i = 0
for a in range(2):
    for b in range(2):
        for c in range(4):            
            int_log2 = int_log2_headers[a] + int_log2_body
            FFT_bitreverse = FFT_bitreverse_headers[b] + FFT_bitreverse_body
            FFT_transform_internal = FFT_transform_internal_headers[c] + FFT_transform_internal_body
            out = head + int_log2 + FFT_bitreverse + FFT_transform_internal
            with open("loc" + str(i) + ".py", "w+") as f:
                f.write(out)
            i += 1
            
