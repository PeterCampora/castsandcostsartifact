echo "Herder parameters tests"

echo "2 parameters"
cd 2Params
cat log-dynamic.txt
cd ..
echo ""

echo "4 parameters"
cd 4Params
cat log-dynamic.txt
cd ..
echo ""

echo "6 parameters"
cd 6Params
cat log-dynamic.txt
cd ..
echo ""

echo "8 parameters"
cd 8Params
cat log-dynamic.txt
cd ..
echo ""

echo "10 parameters"
cd 10Params
cat log-dynamic.txt
cd ..
echo ""

echo "12 parameters"
cd 12Params
cat log-dynamic.txt
cd ..
echo ""

echo "~50 parameters"
cd 50Params
cat log-dynamic.txt
cd ..
echo ""

echo "~100 parameters"
cd 100Params
cat log-dynamic.txt
cd ..
echo ""

echo "~200 parameters"
cd 200Params
cat log-dynamic.txt
cd ..
echo ""

echo "~400 parameters"
cd 400Params
cat log-dynamic.txt
cd ..
echo ""

echo "~800 parameters"
cd 800Params
cat log-dynamic.txt
cd ..
echo ""

echo "------------------------------------------------------------------------------------"
echo "Herder lines of code"
echo "~80 loc"
cd 80LOC
cat log-dynamic.txt
cd ..
echo ""

echo "~100 loc"
cd 100LOC
cat log-dynamic.txt
cd ..
echo ""

echo "~250 loc"
cd 250LOC
cat log-dynamic.txt
cd ..
echo ""

echo "~1000 loc"
cd 1000LOC
cat log-dynamic.txt
cd ..
echo ""

echo "~2500 loc"
cd 2500LOC
cat log-dynamic.txt
cd ..
echo ""

echo "~5000 loc"
cd 5000LOC
cat log-dynamic.txt
cd ..
echo ""
