echo "Bruteforce parameters tests"

echo "2 parameters"
cd 2Params
python3 sum.py
cd ..
echo ""

echo "4 parameters"
cd 4Params
python3 sum.py
cd ..
echo ""

echo "6 parameters"
cd 6Params
python3 sum.py
cd ..
echo ""

echo "8 parameters"
cd 8Params
python3 sum.py
cd ..
echo ""

echo "10 parameters"
cd 10Params
python3 sum.py
cd ..
echo ""

echo "12 parameters"
cd 12Params
python3 sum.py
cd ..
echo ""

echo "------------------------------------------------------------------------------------"
echo "Bruteforce lines of code"
echo "~80 loc"
cd 80LOC
python3 sum.py
cd ..
echo ""

echo "~100 loc"
cd 100LOC
python3 sum.py
cd ..
echo ""

echo "~250 loc"
cd 250LOC
python3 sum.py
cd ..
echo ""

echo "~1000 loc"
cd 1000LOC
python3 sum.py
cd ..
echo ""

echo "~2500 loc"
cd 2500LOC
python3 sum.py
cd ..
echo ""

echo "~5000 loc"
cd 5000LOC
python3 sum.py
cd ..
echo ""
