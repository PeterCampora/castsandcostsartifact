def one(x,y)->float:
    (a,b) = y
    return x + 1.0

def two(x:float,y)->float:
    (a,b) = y
    return x + 1.0

def three(x,y)->float:
    (a,b) = y
    return x + 1.0


def main():
    one(1.0, (1,2))
    two(1.0, (1,2))
    three(1.0, (1,2))


