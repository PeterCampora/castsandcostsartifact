one_headers = ["def one(x,y)->float:",
               "def one(x:float,y)->float:",
               "def one(x,y:Tuple(Dyn,Dyn))->float:",
               "def one(x:float,y:Tuple(Dyn,Dyn))->float:"]
one_body = """
    (a,b) = y
    return x + 1.0

"""

two_headers = ["def two(x,y)->float:",
               "def two(x:float,y)->float:",
               "def two(x,y:Tuple(Dyn,Dyn))->float:",
               "def two(x:float,y:Tuple(Dyn,Dyn))->float:"]
two_body = """
    (a,b) = y
    return x + 1.0

"""

three_headers = ["def three(x,y)->float:",
               "def three(x:float,y)->float:",
               "def three(x,y:Tuple(Dyn,Dyn))->float:",
               "def three(x:float,y:Tuple(Dyn,Dyn))->float:"]
three_body = """
    (a,b) = y
    return x + 1.0

"""

four_headers = ["def four(x,y)->float:",
               "def four(x:float,y)->float:",
               "def four(x,y:Tuple(Dyn,Dyn))->float:",
               "def four(x:float,y:Tuple(Dyn,Dyn))->float:"]
four_body = """
    (a,b) = y
    return x + 1.0

"""

five_headers = ["def five(x,y)->float:",
               "def five(x:float,y)->float:",
               "def five(x,y:Tuple(Dyn,Dyn))->float:",
               "def five(x:float,y:Tuple(Dyn,Dyn))->float:"]
five_body = """
    (a,b) = y
    return x + 1.0

"""

six_headers = ["def six(x,y)->float:",
               "def six(x:float,y)->float:",
               "def six(x,y:Tuple(Dyn,Dyn))->float:",
               "def six(x:float,y:Tuple(Dyn,Dyn))->float:"]
six_body = """
    (a,b) = y
    return x + 1.0

"""

seven_headers = ["def seven(x,y)->float:",
               "def seven(x:float,y)->float:",
               "def seven(x,y:Tuple(Dyn,Dyn))->float:",
               "def seven(x:float,y:Tuple(Dyn,Dyn))->float:"]
seven_body = """
    (a,b) = y
    return x + 1.0

"""

rest = """
def main():
    one(1.0, (1,2))
    two(1.0, (1,2))
    three(1.0, (1,2))
    four(1.0, (1,2))
    five(1.0, (1,2))
    six(1.0, (1,2))

"""

i = 0

for b in range(4):
    for c in range(4):
        for d in range(4):
            for e in range(4):
                for f in range(4):
                    for g in range(4):
                        i += 1
                        one = one_headers[g] + one_body
                        two = two_headers[f] + two_body
                        three = three_headers[e] + three_body
                        four = four_headers[d] + four_body
                        five = five_headers[c] + five_body
                        six = six_headers[b] + six_body
                        prog = one + two + three + four + five + six + rest
                        with open("twelve" + str(i) + ".py", "w+") as fil:
                            fil.write(prog)

                                
