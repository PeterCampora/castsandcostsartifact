def one(x:float,y:Tuple(Dyn,Dyn))->float:
    (a,b) = y
    return x + 1.0

def two(x:float,y)->float:
    (a,b) = y
    return x + 1.0

def three(x,y)->float:
    (a,b) = y
    return x + 1.0

def four(x:float,y)->float:
    (a,b) = y
    return x + 1.0

def five(x:float,y)->float:
    (a,b) = y
    return x + 1.0


def main():
    one(1.0, (1,2))
    two(1.0, (1,2))
    three(1.0, (1,2))
    four(1.0, (1,2))
    five(1.0, (1,2))


