cd 2Params
sh runBenches.sh
cd ..

cd 4Params
sh runBenches.sh
cd ..

cd 6Params
sh runBenches.sh
cd ..

cd 8Params
sh runBenches.sh
cd ..

cd 10Params
sh runBenches.sh
cd ..

cd 12Params
sh runBenches.sh
cd ..

cd 80LOC
sh runBenches.sh
cd ..

cd 100LOC
sh runBenches.sh
cd ..

cd 250LOC
sh runBenches.sh
cd ..

cd 1000LOC
sh runBenches.sh
cd ..

cd 2500LOC
sh runBenches.sh
cd ..

cd 5000LOC
sh runBenches.sh
cd ..
