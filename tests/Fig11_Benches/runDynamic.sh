echo "Running the Figure11 (left) parameters tests on the dynamic configurations"

echo "2 parameters"
cd 2Params
rm log-dynamic.txt
retic --guarded two1.py >> log-dynamic.txt
cd ..
echo ""

echo "4 parameters"
cd 4Params
rm log-dynamic.txt
retic --guarded four1.py >> log-dynamic.txt
cd ..
echo ""

echo "6 parameters"
cd 6Params
rm log-dynamic.txt
retic --guarded six1.py >> log-dynamic.txt
cd ..
echo ""

echo "8 parameters"
cd 8Params
rm log-dynamic.txt
retic --guarded eight1.py >> log-dynamic.txt
cd ..
echo ""

echo "10 parameters"
cd 10Params
rm log-dynamic.txt
retic --guarded ten1.py >> log-dynamic.txt
cd ..
echo ""

echo "12 parameters"
cd 12Params
rm log-dynamic.txt
retic --guarded twelve1.py >> log-dynamic.txt
cd ..
echo ""

echo "~50 parameters"
cd 50Params
rm log-dynamic.txt
retic --guarded c50.py >> log-dynamic.txt
cd ..
echo ""

echo "~100 parameters"
cd 100Params
rm log-dynamic.txt
retic --guarded c100.py >> log-dynamic.txt
cd ..
echo ""

echo "~200 parameters"
cd 200Params
rm log-dynamic.txt
retic --guarded c200.py >> log-dynamic.txt
cd ..
echo ""

echo "~400 parameters"
cd 400Params
rm log-dynamic.txt
retic --guarded c400.py >> log-dynamic.txt
cd ..
echo ""

echo "~800 parameters"
cd 800Params
rm log-dynamic.txt
retic --guarded c800.py >> log-dynamic.txt
cd ..
echo ""

echo "------------------------------------------------------------------------------------"
echo "Running the Figure11 (right) lines of code tests for the dynamic configuration"
echo "~80 loc"
cd 80LOC
rm log-dynamic.txt
retic --guarded loc0.py >> log-dynamic.txt
cd ..
echo ""

echo "~100 loc"
cd 100LOC
rm log-dynamic.txt
retic --guarded loc0.py >> log-dynamic.txt
cd ..
echo ""

echo "~250 loc"
cd 250LOC
rm log-dynamic.txt
retic --guarded loc0.py >> log-dynamic.txt
cd ..
echo ""

echo "~1000 loc"
cd 1000LOC
rm log-dynamic.txt
retic --guarded loc0.py >> log-dynamic.txt
cd ..
echo ""

echo "~2500 loc"
cd 2500LOC
rm log-dynamic.txt
retic --guarded loc0.py >> log-dynamic.txt
cd ..
echo ""

echo "~5000 loc"
cd 5000LOC
rm log-dynamic.txt
retic --guarded loc0.py >> log-dynamic.txt
cd ..
echo ""
